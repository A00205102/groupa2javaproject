package dao;

import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.JOptionPane;

import ConnectDatabase.Conn_db;
import classFolder.Student;

public class DaoStudent extends Conn_db {
	public boolean register(String usern, String passw, double funds) {
		boolean res = false;
		try {
			boolean res1 = writeInSql(usern, passw, funds);
			if (res1 == true) {
				res = true;
			} else {
				res = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return res;
	}

	public boolean writeInSql(String User, String Pass, double fines)
			throws Exception {

		Statement stmt = connection("jdbc:mysql://127.0.0.1:3307/Agile");
		String sql = "insert into UserStudent values(null,'" + User + "','"
				+ Pass + "',0.0," + fines + ")";

		stmt.executeUpdate(sql);
		System.out.println(sql);
		int rw = stmt.executeUpdate(sql);
		if (rw <= 0) {
			JOptionPane.showMessageDialog(null, "Register failed");
			return false;
		} else {
			JOptionPane.showMessageDialog(null, "Register successfully");
			return true;
		}
	}

	public boolean login(String usern, String passw) {
		boolean res = false;
		if (usern.equals(""))
			JOptionPane.showMessageDialog(null, "Please insert username");
		else if (passw.equals(""))
			JOptionPane.showMessageDialog(null, "Please insert password");
		else {

			try {
				boolean com = compareWithSql(usern, passw);
				if (com == true) {
					JOptionPane.showMessageDialog(null, "Log in successfully!");
					res = true;
				} else {
					JOptionPane
							.showMessageDialog(null,
									"User name or password is incorrect,please try it again!");
					res = false;
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}

		return res;

	}

	public boolean compareWithSql(String UserName, String PassWord)
			throws Exception {
		String sql;
		boolean res = false;
		Statement stmt2 = connection("jdbc:mysql://127.0.0.1:3306/Agile");
		sql = "select * from UserStudent";

		ResultSet rs = stmt2.executeQuery(sql);
		while (rs.next()) {
			String username = rs.getString(2);
			String password = rs.getString(3);
			if ((username.equals(UserName) && password.equals(PassWord))) {
				res = true;
			} else {
				res = false;
			}
		}
		return res;
	}

	public Student viewDetails(String user) throws Exception {
		Student s = new Student();
		Statement stmt2 = connection("jdbc:mysql://127.0.0.1:3307/Agile");
		String sql = "select * from UserStudent Where Student_Username = '"
				+ user + "';";

		ResultSet rs = stmt2.executeQuery(sql);
		while (rs.next()) {
			String username = rs.getString(2);
			Double fines = Double.parseDouble(rs.getString(4));
			Double funds = Double.parseDouble(rs.getString(5));
			s.setUsername(username);
			s.setFines(fines);
			s.setFunds(funds);
		}

		return s;
	}

}
