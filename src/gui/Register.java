package gui;

import java.awt.FlowLayout;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import dao.Register_db;

public class Register extends JFrame {
	JLabel title;
	JLabel usernameLabel, pass_wordLabel;
	JButton okButton, resetButton, backButton;
	JTextField usernameText, pass_wordText;
	Box baseBox1, baseBox2, box1, box2, box3; // ��ע��ҳ�����Box���ַ�ʽ��
	// JPanel pane1,pane2;

	Register_db regist;

	public Register() {
		init();
	}

	void init() {
		setLayout(new FlowLayout());
		title = new JLabel("Welcome to Library Management System");
		usernameLabel = new JLabel("username");
		pass_wordLabel = new JLabel("password");
		usernameText = new JTextField(10);
		pass_wordText = new JTextField(20);
		okButton = new JButton("Confirm");
		resetButton = new JButton("Reset");
		backButton = new JButton("Back");

		regist = new Register_db();

		// lab = new JLabel("�û�ע��ҳ��");

		box1 = Box.createVerticalBox();

		box1.add(usernameLabel);
		box1.add(Box.createVerticalStrut(8));
		box1.add(pass_wordLabel);
		box2 = Box.createVerticalBox();
		box2.add(usernameText);
		box2.add(Box.createVerticalStrut(8));
		box2.add(pass_wordText);
		box3 = Box.createHorizontalBox();
		box3.add(okButton);
		box3.add(Box.createHorizontalStrut(15));
		box3.add(resetButton);
		box3.add(backButton);
		baseBox1 = Box.createHorizontalBox();
		baseBox1.add(box1);
		baseBox1.add(Box.createHorizontalStrut(8));
		baseBox1.add(box2);

		baseBox2 = Box.createVerticalBox();
		baseBox2.add(title);
		baseBox2.add(baseBox1);
		baseBox2.add(Box.createVerticalStrut(10));
		baseBox2.add(box3);
		add(baseBox2);

		okButton.addActionListener(regist);
		resetButton.addActionListener(regist);
		backButton.addActionListener(regist);

		regist.setusernameField(usernameText);
		regist.setpass_wordField(pass_wordText);
		regist.setokButton(okButton);
		regist.setresetButton(resetButton);
		regist.setbackButton(backButton);

		setBounds(200, 200, 500, 400);
		setVisible(true);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setTitle("Register interface");
	}

	// void registAction(){
	//
	// }

	// public static void main(String[] args) {
	// register re = new register();
	// }
}