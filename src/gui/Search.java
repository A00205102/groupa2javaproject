package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import classFolder.Book;
import classFolder.Journal;
import dao.DaoBook;
import dao.DaoJournal;
import testing.ExceptionHandlerClass;

import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class Search {

	private JFrame frame;
	private JTextField nametxb;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Search window = new Search();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Search() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Search");
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		JLabel lblPleaseEnterThe = new JLabel(
				"Please Enter the name of the Book/Journal ");
		lblPleaseEnterThe.setBounds(70, 106, 408, 15);
		panel.add(lblPleaseEnterThe);

		JLabel lblYouWantTo = new JLabel("you want to search for");
		lblYouWantTo.setBounds(135, 133, 176, 15);
		panel.add(lblYouWantTo);

		JRadioButton rdbtnBook = new JRadioButton("Book");
		buttonGroup.add(rdbtnBook);
		rdbtnBook.setBounds(119, 75, 74, 23);
		panel.add(rdbtnBook);

		JRadioButton rdbtnJournal = new JRadioButton("Journal");
		buttonGroup.add(rdbtnJournal);
		rdbtnJournal.setBounds(229, 75, 82, 23);
		panel.add(rdbtnJournal);

		nametxb = new JTextField();
		nametxb.setBounds(151, 167, 114, 19);
		panel.add(nametxb);
		nametxb.setColumns(10);

		JButton btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (rdbtnBook.isSelected()) {
					String bname = nametxb.getText();
					DaoBook book = new DaoBook();
					Book b = new Book();
					String bookDetails;
					boolean resB = false;
					try {
						resB = book.searchBook(bname);
					} catch (ExceptionHandlerClass e) {
						e.printStackTrace();
					}
					if (resB == true) {
						try {
							b = book.bookDetails(bname);
						} catch (ClassNotFoundException | SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						bookDetails = "Book details: \n" + "Book Id: "
								+ b.getID() + "\n" + "Name: " + b.getName()
								+ "\n" + "Author: " + b.getAuthor() + "\n"
								+ "Library Number: " + b.getLibraryNo() + "\n"
								+ "ISBN Number: " + b.getISBN_No() + "\n"
								+ "Ref Number: " + b.getRef_No() + "\n"
								+ "Release Year: " + b.getrelease_Year() + "\n";
						JOptionPane.showMessageDialog(frame,
								"The Book you have Searched " + bname
										+ " book is in the Database.\n"
										+ bookDetails, "Book Search",
								JOptionPane.INFORMATION_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(frame,
								"The Book you have Searched " + bname
										+ " book is NOT in the Database.",
								"Book Search", JOptionPane.ERROR_MESSAGE);
					}

				}
				if (rdbtnJournal.isSelected()) {
					DaoJournal journal = new DaoJournal();
					String jname = nametxb.getText();
					Journal j = new Journal();
					String journalDetails;
					boolean resJ = false;
					resJ = journal.searchJournal(jname);
					if (resJ == true) {
						try {
							j = journal.journalDetails(jname);
						} catch (ClassNotFoundException | SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						journalDetails = "Journal details: \n" + "Journal Id: "
								+ j.getID() + "\n" + "Name: " + j.getName()
								+ "\n" + "Author: " + j.getAuthor() + "\n"
								+ "Library Number: " + j.getLibraryNo() + "\n"
								+ "ISBN Number: " + j.getISBN_No() + "\n"
								+ "Ref Number: " + j.getRef_No() + "\n"
								+ "Release Year: " + j.getrelease_Year() + "\n";
						JOptionPane.showMessageDialog(frame,
								"The Journal you have Searched " + jname
										+ " journal is in the Database.\n"
										+ journalDetails, "Journal Search",
								JOptionPane.INFORMATION_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(frame,
								"The Journal you have Searched " + jname
										+ " journal is NOT in the Database.",
								"Journal Search", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		btnSubmit.setBounds(246, 198, 117, 25);
		panel.add(btnSubmit);

		JLabel lblSearch = new JLabel("Search ");
		lblSearch.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 33));
		lblSearch.setBounds(133, 12, 161, 44);
		panel.add(lblSearch);

		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
			}
		});
		btnBack.setBounds(51, 198, 117, 25);
		panel.add(btnBack);

	}
}
