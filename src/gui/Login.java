package gui;

import java.awt.CardLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.*;

import dao.Login_db;

public class Login extends JFrame {
	// JLabel accountL,nameL;

	JLabel title;
	JTextField usernameT, pass_wordT;
	JButton okB, registB, backB;
	Box baseB1, baseB2, box1, box2, box3; // �˵�¼ҳ�����Box���ַ�ʽ��
	Login_db log;

	public Login() {
		init();
	}

	void init() {
		log = new Login_db();

		title = new JLabel("Welcome to Library Management System");

		usernameT = new JTextField(10);
		pass_wordT = new JTextField(20);
		okB = new JButton("Log in");
		registB = new JButton("Register");
		backB = new JButton("Back To Selection");

		box1 = Box.createVerticalBox();
		box1.add(new JLabel("username"));
		box1.add(Box.createVerticalStrut(8));
		box1.add(new JLabel("password"));

		box2 = Box.createVerticalBox();
		box2.add(usernameT);
		box2.add(Box.createVerticalStrut(8));
		box2.add(pass_wordT);

		box3 = Box.createHorizontalBox();
		box3.add(okB);
		box3.add(Box.createHorizontalStrut(20));
		box3.add(registB);
		box3.add(backB);

		baseB1 = Box.createHorizontalBox();
		baseB1.add(box1);
		baseB1.add(Box.createHorizontalStrut(8));
		baseB1.add(box2);

		baseB2 = Box.createVerticalBox();
		baseB2.add(title);
		baseB2.add(baseB1);
		baseB2.add(Box.createVerticalStrut(10));
		baseB2.add(box3);

		okB.addActionListener(log);
		registB.addActionListener(log);
		backB.addActionListener(log);

		log.setaccountT(usernameT);
		log.setnameT(pass_wordT);
		log.setButton(okB, registB, backB);

		add(baseB2);
		setLayout(new FlowLayout());
		setBounds(200, 150, 500, 400);
		setVisible(true);
		setTitle("Log in interface");
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}

	public static void main(String[] args) {
		Login lo = new Login();
	}
}