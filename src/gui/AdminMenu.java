package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Window;

import javax.swing.JButton;
import javax.swing.JComponent;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AdminMenu {

	private JFrame frame;
	private JFrame AddBookWindow;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminMenu window = new AdminMenu();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AdminMenu() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Admin Menu");
		frame.setBounds(100, 100, 450, 348);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		JLabel lblWelcomeToAdmin = new JLabel("Welcome to Admin Menu");
		lblWelcomeToAdmin.setFont(new Font("Dialog", Font.BOLD, 20));
		lblWelcomeToAdmin.setBounds(79, 12, 299, 42);
		panel.add(lblWelcomeToAdmin);

		JButton btnAddBook = new JButton("Add Book");
		btnAddBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AddBookGUI s = new AddBookGUI();
				s.main(null);
				frame.setVisible(false);
			}
		});
		btnAddBook.setBounds(236, 68, 185, 25);
		panel.add(btnAddBook);

		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Search s = new Search();
				s.main(null);
			}
		});
		btnSearch.setBounds(236, 140, 185, 25);
		panel.add(btnSearch);

		JButton btnAddJournal = new JButton("Add Journal");
		btnAddJournal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddJournalGUI aj = new AddJournalGUI();
				aj.main(null);
				frame.setVisible(false);
			}
		});
		btnAddJournal.setBounds(30, 68, 179, 25);
		panel.add(btnAddJournal);

		JButton btnUpdateBookk = new JButton("Update Book");
		btnUpdateBookk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UpdateBookGUI ub = new UpdateBookGUI();
				ub.main(null);
				frame.setVisible(false);
			}
		});
		btnUpdateBookk.setBounds(236, 104, 185, 25);
		panel.add(btnUpdateBookk);

		JButton btnUpdateJournal = new JButton("Update Journal");
		btnUpdateJournal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UpdateJournalGUI uj = new UpdateJournalGUI();
				uj.main(null);
				frame.setVisible(false);
			}
		});
		btnUpdateJournal.setBounds(30, 104, 179, 25);
		panel.add(btnUpdateJournal);

		JButton btnNewButton = new JButton("Logout");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Login l = new Login();
				l.main(null);
				frame.setVisible(false);
			}
		});
		btnNewButton.setBounds(236, 212, 186, 25);
		panel.add(btnNewButton);

		JButton btnSendNotification = new JButton("Send Notification");
		btnSendNotification.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminSendRequestNotification asrn = new AdminSendRequestNotification();
				asrn.main(null);
				frame.setVisible(false);
			}
		});
		btnSendNotification.setBounds(30, 140, 179, 25);
		panel.add(btnSendNotification);

		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		btnExit.setBounds(136, 273, 179, 25);
		panel.add(btnExit);

		JButton btnAddFine = new JButton("Add Fine ");
		btnAddFine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddFineGUI addFine = new AddFineGUI();
				addFine.main(null);
			}
		});
		btnAddFine.setBounds(30, 176, 178, 25);
		panel.add(btnAddFine);

		JButton btnViewRequests = new JButton("View Requests");
		btnViewRequests.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ViewReqGUI view = new ViewReqGUI();
				view.main(null);
				frame.setVisible(false);
			}
		});
		btnViewRequests.setBounds(236, 176, 185, 25);
		panel.add(btnViewRequests);
		
		JButton btnSendExpiryNotification = new JButton("Send Expiry Notification");
		btnSendExpiryNotification.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminSendReturnNotification asrn = new AdminSendReturnNotification();
				asrn.main(null);
				frame.setVisible(false);
			}
		});
		btnSendExpiryNotification.setBounds(30, 212, 179, 23);
		panel.add(btnSendExpiryNotification);
	}
}
