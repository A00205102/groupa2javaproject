package ConnectDatabase;

import java.sql.Statement;

public class DaoviewReq {
	private Statement stmt;
	private Conn_db con;

	public DaoviewReq(Conn_db conDB) {
		con = conDB;
	}

	public int viewRequest(QueryTableModel tableModel, String option)
			throws Exception {

		stmt = con.connection("jdbc:mysql://127.0.0.1:3307/Agile");
		try {
			tableModel.refreshFromDB(stmt, option);
		} catch (Exception e)

		{
			throw new Exception("View Request Exception");
		}
		return 1;
	}
}
