package testing;


import dao.DaoFund;

import junit.framework.TestCase;

public class AddFundsTest_Hank extends TestCase {
	    // Test number: 1
		// Test objective: Test valid input
		// Input(s): student id = 1,name=a,fine=1.0
		// Expected Output(s): Boolean = true
		// Added by: Hank
		public void test001()  {
			DaoFund testObject = new DaoFund();
			try {
				assertTrue(testObject.AddFund(1, "a", 1.0));
			} catch (Exception e) {
				e.printStackTrace();
			
			}
		}
		        // Test number: 2
				// Test objective: Test valid input
				// Input(s): student id = 100,name=a,fine=1.0
				// Expected Output(s): Boolean = true
				// Added by: Hank
				public void test002()  {
					DaoFund testObject = new DaoFund();
					try {
						assertTrue(testObject.AddFund(100, "a", 1.0));
					} catch (Exception e) {
						e.printStackTrace();
					
					}
				}
				// Test number: 3
				// Test objective: Test valid input
				// Input(s): student id = MAX_INT,name=a,fine=1.0
				// Expected Output(s): Boolean = true
				// Added by: Hank
				public void test003()  {
					DaoFund testObject = new DaoFund();
					try {
						assertTrue(testObject.AddFund(Integer.MAX_VALUE, "a", 1.0));
					} catch (Exception e) {
						e.printStackTrace();
					
					}
				}
				// Test number: 4
				// Test objective: Test valid input
				// Input(s): student id = 1,name=a,fine=0.1
				// Expected Output(s): Boolean = true
				// Added by: Hank
				public void test004()  {
					DaoFund testObject = new DaoFund();
					try {
						assertTrue(testObject.AddFund(1, "a", 0.1));
					} catch (Exception e) {
						e.printStackTrace();
					
					}
				}
				// Test number: 5
				// Test objective: Test valid input
				// Input(s): student id = 1,name=a,fine=100.0
				// Expected Output(s): Boolean = true
				// Added by: Hank
				public void test005()  {
					DaoFund testObject = new DaoFund();
					try {
						assertTrue(testObject.AddFund(1, "a", 100.0));
					} catch (Exception e) {
						e.printStackTrace();
					
					}
				}
				// Test number:6
				// Test objective: Test invalid input
				// Input(s): student id = -1,name=a,fine=1.0
				// Expected Output(s): Boolean = true
				// Added by: Hank
				public void test006()  {
					DaoFund testObject = new DaoFund();
					try {
						assertFalse(testObject.AddFund(-1, "a", 1.0));
					} catch (Exception e) {
						e.printStackTrace();
				
					}
				}
				// Test number:7
				// Test objective: Test invalid input
				// Input(s): student id = MIN_INT,name=a,fine=1.0
				// Expected Output(s): Boolean = true
				// Added by: Hank
				public void test007()  {
					DaoFund testObject = new DaoFund();
					try {
						assertFalse(testObject.AddFund(Integer.MIN_VALUE, "a", 1.0));
					} catch (Exception e) {
						e.printStackTrace();
				
					}
				}
				// Test number:8
				// Test objective: Test invalid input
				// Input(s): student id = 1,name=a,fine=-1
				// Expected Output(s): Boolean = true
				// Added by: Hank
				public void test008()  {
					DaoFund testObject = new DaoFund();
					try {
						assertFalse(testObject.AddFund(1, "a", -1));
					} catch (Exception e) {
						e.printStackTrace();
				
					}
				}
				// Test number:9
				// Test objective: Test invalid input
				// Input(s): student id = 1,name=a,fine=-100
				// Expected Output(s): Boolean = true
				// Added by: Hank
				public void test009()  {
					DaoFund testObject = new DaoFund();
					try {
						assertFalse(testObject.AddFund(1, "a", -100.0));
					} catch (Exception e) {
						e.printStackTrace();
				
					}
				}
				// Test number:10
				// Test objective: Test invalid input
				// Input(s): student id = 1,name=,fine=-100
				// Expected Output(s): Boolean = true
				// Added by: Hank
				public void test0010()  {
					DaoFund testObject = new DaoFund();
					try {
						assertFalse(testObject.AddFund(1, "", -100.0));
					} catch (Exception e) {
						e.printStackTrace();
				
					}
				}
				// Test number:11
				// Test objective: Test invalid input
				// Input(s): student id = 0,name=a,fine=1
				// Expected Output(s): Boolean = true
				// Added by: Hank
				public void test0011()  {
					DaoFund testObject = new DaoFund();
					try {
						assertFalse(testObject.AddFund(0, "a", 1));
					} catch (Exception e) {
						e.printStackTrace();
				
					}
				}

}

