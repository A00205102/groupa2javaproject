package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import ConnectDatabase.Conn_db;
import classFolder.Book;
import testing.ExceptionHandlerClass;

public class DaoBook extends Conn_db {

	private Connection con = null;
	private static Statement stmt = null;
	private ResultSet rs = null;

	public Book bookDetails(String name) throws ClassNotFoundException,
			SQLException {
		Book b = new Book();

		connection("jdbc:mysql://127.0.0.1:3307/Agile");
		String cmd = "select * from Books where Book_Name = '" + name + "';";
		rs = stmt.executeQuery(cmd);

		while (rs.next()) {
			int Bid = Integer.parseInt(rs.getString(1));
			String Bname = rs.getString(2);
			String author = rs.getString(3);
			int libNo = Integer.parseInt(rs.getString(4));
			int isbnNo = Integer.parseInt(rs.getString(5));
			int refNo = Integer.parseInt(rs.getString(6));
			int year = Integer.parseInt(rs.getString(7));
			b.setID(Bid);
			b.setName(Bname);
			b.setAuthor(author);
			b.setLibraryNo(libNo);
			b.setISBN_No(isbnNo);
			b.setRef_No(refNo);
			b.setrelease_Year(year);
		}
		return b;
	}

	// search book method added by Patrycja
	// Partial Product Skeleton Code
	public boolean searchBook(String name) throws ExceptionHandlerClass {
		// throw new RuntimeException();
		Conn_db obj = new Conn_db();
		boolean res = false;
		try {
			stmt = (Statement) obj
					.connection("jdbc:mysql://127.0.0.1:3307/Agile");
			String cmd = "select * from Books where Book_Name = '" + name
					+ "';";
			rs = stmt.executeQuery(cmd);

			while (rs.next()) {
				String Bname = rs.getString(2);
				if (Bname != null) {
					res = true;
				} else {
					res = false;
				}
			}
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}

		return res;
	}

	// add book method added by Mark
	// Partial Product Skeleton Code
	public boolean addBook(String name, String author, int libNum, int isbn,
			int refNum, int relYear) throws ExceptionHandlerClass {

		Conn_db addB = new Conn_db();
		try {

			if (relYear > 9999) {
				return false;
			}
			if (relYear < 0001) {
				return false;
			}

			if (refNum > 999999) {
				return false;
			}
			if (refNum < 1) {
				return false;
			}
			if (isbn > 999999) {
				return false;
			}
			if (isbn < 1) {
				return false;
			}

			if (libNum > 999999) {
				return false;
			}
			if (libNum < 1) {
				return false;
			}
			if (name.length() > 20) {
				return false;
			} else if (author.length() > 20) {
				return false;
			} else

				stmt = (Statement) addB
						.connection("jdbc:mysql://127.0.0.1:3307/Agile");
			String cmd = "INSERT INTO Books VALUES ( null, '" + name + "', '"
					+ author + "', " + libNum + ", " + isbn + ", " + refNum
					+ "," + relYear + ");";
			stmt.executeUpdate(cmd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	// update book method added by Shane
	// Partial Product Skeleton Code
	public static boolean updateBook(int id, String name, String author,
			int libraryNum, int isbn, int refNum, int year)
			throws ExceptionHandlerClass, Exception {
		Conn_db conn = new Conn_db();
		stmt = (Statement) conn.connection("jdbc:mysql://127.0.0.1:3307/Agile");
		if (name.equals("")) {
			throw new ExceptionHandlerClass("Please enter a book name");
		}
		if (name.length() > 20) {
			throw new ExceptionHandlerClass(
					"Maximum character amount exceeded for book name (20)");
		}
		if (author.equals("")) {
			throw new ExceptionHandlerClass("Please enter authors name");
		}
		if (author.length() > 20) {
			throw new ExceptionHandlerClass(
					"Maximum character amount exceeded for author name (20)");
		}
		if (libraryNum == 0) {
			throw new ExceptionHandlerClass(
					"Invalid library number entered, please enter a value greater than zero");
		}
		if (libraryNum <= -1) {
			throw new ExceptionHandlerClass("Invalid Library number entered");
		}
		if (isbn == 0) {
			throw new ExceptionHandlerClass(
					"Invalid ISBN number entered, please enter a value greater than zero");
		}
		if (isbn <= -1) {
			throw new ExceptionHandlerClass("Invalid ISBN number entered");
		}
		if (refNum == 0) {
			throw new ExceptionHandlerClass(
					"Invalid reference number entered, please enter a value greater than zero");
		}
		if (refNum <= -1) {
			throw new ExceptionHandlerClass("Invalid reference Number entered");
		}
		if (year == 0) {
			throw new ExceptionHandlerClass(
					"Invalid year entered, please enter a value greater than zero");
		}
		if (year <= -1) {
			throw new ExceptionHandlerClass("Invalid year entered");
		}
		if (id == 0) {
			throw new ExceptionHandlerClass(
					"Invalid Book ID entered, please enter a value greater than zero");
		}
		if (id < 0) {
			throw new ExceptionHandlerClass("Invalid Book ID entered");
		}
		try {
			String query = "UPDATE Books SET Book_Name='" + name
					+ "', Author='" + author + "', Library_Number='"
					+ libraryNum + "', ISBN_Number='" + isbn
					+ "', Ref_Number='" + refNum + "', Release_Year='" + year
					+ "' WHERE Book_id=" + id + ";";
			stmt.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return true;

	}

	public boolean placeBook(int id, String title, String author, int isbn)
			throws ExceptionHandlerClass, Exception {
		Conn_db connection = new Conn_db();
		Statement stmt2;
		if (id == 0) {
			throw new ExceptionHandlerClass(
					"Invalid Book ID entered, please enter a value greater than zero");
		} else if (id < 0) {
			throw new ExceptionHandlerClass("Invalid Book ID entered");
		} else if (id > 0) {
			try {
				stmt2 = connection
						.connection("jdbc:mysql://127.0.0.1:3307/Agile");

				String updateTemp = "INSERT INTO req_Book(id,title,author,ISBN) VALUES("
						+ id
						+ ",'"
						+ title
						+ "','"
						+ author
						+ "',"
						+ isbn
						+ ");";
				System.out.println(updateTemp);
				stmt2.executeUpdate(updateTemp);
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
			return true;
		} else {
			return false;
		}
	}

}
