package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;

import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import dao.DaoFine;

import java.awt.Window.Type;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AddFineGUI {

	private JFrame AddFineWindow;
	private JTextField idTextBox;
	public static JTextField studentNameTextBox;
	public static JTextField FineTextBox;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddFineGUI window = new AddFineGUI();
					window.AddFineWindow.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AddFineGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		AddFineWindow = new JFrame();
		AddFineWindow.setTitle("Add fine");
		AddFineWindow
				.setFont(new Font("Arial Rounded MT Bold", Font.PLAIN, 12));
		AddFineWindow.setBounds(100, 100, 300, 325);
		AddFineWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		AddFineWindow.getContentPane().setLayout(null);

		JLabel idLabel = new JLabel("Enter Id of student:");
		idLabel.setBounds(23, 11, 132, 14);
		AddFineWindow.getContentPane().add(idLabel);

		idTextBox = new JTextField();
		idTextBox.setBounds(152, 8, 122, 20);
		AddFineWindow.getContentPane().add(idTextBox);
		idTextBox.setColumns(10);

		studentNameTextBox = new JTextField();
		studentNameTextBox.setBounds(152, 63, 122, 20);
		AddFineWindow.getContentPane().add(studentNameTextBox);
		studentNameTextBox.setColumns(10);

		FineTextBox = new JTextField();
		FineTextBox.setBounds(152, 125, 122, 20);
		AddFineWindow.getContentPane().add(FineTextBox);
		FineTextBox.setColumns(10);

		JButton backButton = new JButton("Back");
		backButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AddFineWindow.setVisible(false);
				// AdminMenu menu = new AdminMenu();
				// menu.main(null);
			}
		});
		backButton.setBounds(10, 247, 122, 23);
		AddFineWindow.getContentPane().add(backButton);

		JButton AddFineButton = new JButton("Add Fine");
		AddFineButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int id = Integer.parseInt(idTextBox.getText());
				String name = studentNameTextBox.getText();
				double fine = Double.parseDouble(FineTextBox.getText());

				try {
					DaoFine book = new DaoFine();
					book.AddFine(id, name, fine);
					JOptionPane.showMessageDialog(AddFineButton,
							"Add Fine Successful");
					clearFields();
				}
				// catch (NumberFormatException | DaoBookExceptionHandler e) {
				// JOptionPane.showMessageDialog(AddFineButton, e);
				// }
				catch (Exception e) {
					e.printStackTrace();
				}
			}

			private void clearFields() {
				idTextBox.setText("");
				studentNameTextBox.setText("");
				FineTextBox.setText("");

			}
		});
		AddFineButton.setBounds(152, 249, 122, 23);
		AddFineWindow.getContentPane().add(AddFineButton);

		JLabel studentNameLabel = new JLabel("student Name:");
		studentNameLabel.setBounds(23, 66, 109, 14);
		AddFineWindow.getContentPane().add(studentNameLabel);

		JLabel FineLabel = new JLabel("Fine:");
		FineLabel.setBounds(23, 128, 109, 14);
		AddFineWindow.getContentPane().add(FineLabel);

	}
}
