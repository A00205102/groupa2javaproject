package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainWindow {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		JLabel lblWelcomeToLibrary = new JLabel("Welcome  To Library System");
		lblWelcomeToLibrary.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC,
				18));
		lblWelcomeToLibrary.setBounds(37, 12, 399, 35);
		panel.add(lblWelcomeToLibrary);

		JLabel lblPleaseChooseThe = new JLabel(
				"Please choose the role in which you would like to login");
		lblPleaseChooseThe.setBounds(24, 75, 412, 15);
		panel.add(lblPleaseChooseThe);

		JButton btnStudent = new JButton("Student");
		btnStudent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LoginStu sl = new LoginStu();
				sl.main(null);
			}
		});
		btnStudent.setBounds(37, 152, 117, 25);
		panel.add(btnStudent);

		JButton btnAdmin = new JButton("Admin");
		btnAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Login l = new Login();
				frame.setVisible(false);
			}
		});
		btnAdmin.setBounds(221, 152, 117, 25);
		panel.add(btnAdmin);
	}
}
