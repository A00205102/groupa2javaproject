package testing;

import junit.framework.TestCase;
import dao.Register_db;
import dao.Login_db;

public class Register_LoginTest extends TestCase {
	// Test number: 1
	// Test objective: Test valid input
	// Input(s): username="Lois",password="123"
	// Expected Output(s): Boolean = true
	// Added by: Lois
	public void test001() {
		Register_db testObject = new Register_db();
		try {
			assertTrue(testObject.writeInSql("Lois", "123"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 2
	// Test objective: Test invalid input
	// Input(s): username="Lois",password=""
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test002() {
		Register_db testObject = new Register_db();
		try {
			assertFalse(testObject.writeInSql("Lois", ""));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 3
	// Test objective: Test invalid input
	// Input(s): username="",password="123"
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test003() {
		Register_db testObject = new Register_db();
		try {
			assertFalse(testObject.writeInSql("", "123"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 4
	// Test objective: Test valid input
	// Input(s): username="Lois",password="123"
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test004() {
		Login_db testObject = new Login_db();
		try {
			assertTrue(testObject.compareWithSql("Lois", "123"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 5
	// Test objective: Test invalid input
	// Input(s): username="Lois",password="12345"
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test005() {
		Login_db testObject = new Login_db();
		try {
			assertFalse(testObject.compareWithSql("Lois", "12345"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 6
	// Test objective: Test invalid input
	// Input(s): username="Louis",password="12345"
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test006() {
		Login_db testObject = new Login_db();
		try {
			assertFalse(testObject.compareWithSql("Louis", "12345"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 7
	// Test objective: Test invalid input
	// Input(s): username="",password=""
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test007() {
		Register_db testObject = new Register_db();
		try {
			assertFalse(testObject.writeInSql("", ""));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 8
	// Test objective: Test invalid input
	// Input(s): username="Lois",password=""
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test008() {
		Login_db testObject = new Login_db();
		try {
			assertFalse(testObject.compareWithSql("Lois", ""));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 9
	// Test objective: Test invalid input
	// Input(s): username="",password="123"
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test009() {
		Login_db testObject = new Login_db();
		try {
			assertFalse(testObject.compareWithSql("", "123"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 10
	// Test objective: Test invalid input
	// Input(s): username="",password=""
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test0010() {
		Login_db testObject = new Login_db();
		try {
			assertFalse(testObject.compareWithSql("", ""));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 11
	// Test objective: Test valid input
	// Input(s): username="admin",password="pass"
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test0011() {
		Login_db testObject = new Login_db();
		try {
			assertTrue(testObject.compareWithSql("admin", "pass"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 12
	// Test objective: Test invalid input
	// Input(s): username="admin",password="pass123"
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test0012() {
		Login_db testObject = new Login_db();
		try {
			assertFalse(testObject.compareWithSql("admin", "pass123"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 13
	// Test objective: Test invalid input
	// Input(s): username="admin",password=""
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test0013() {
		Login_db testObject = new Login_db();
		try {
			assertFalse(testObject.compareWithSql("admin", ""));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 14
	// Test objective: Test invalid input
	// Input(s): username="",password="pass"
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test0014() {
		Login_db testObject = new Login_db();
		try {
			assertFalse(testObject.compareWithSql("", "pass"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 15
	// Test objective: Test invalid input
	// Input(s): username="adminnn",password="pass"
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test0015() {
		Login_db testObject = new Login_db();
		try {
			assertFalse(testObject.compareWithSql("adminnn", "pass"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
