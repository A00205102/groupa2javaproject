package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import testing.ExceptionHandlerClass;
import ConnectDatabase.Conn_db;

public class DaoFine extends Conn_db {

	private Connection con = null;
	private static Statement stmt = null;
	private ResultSet rs = null;

	public static boolean AddFine(int id, String name, double fine)
			throws ExceptionHandlerClass, Exception {
		Conn_db conn = new Conn_db();
		stmt = (Statement) conn.connection("jdbc:mysql://127.0.0.1:3307/Agile");
		if (name.equals("")) {
			throw new ExceptionHandlerClass("Please enter a student name");
		}

		if (fine == 0) {
			throw new ExceptionHandlerClass("You didn't add fine");
		}
		if (fine <= -1) {
			throw new ExceptionHandlerClass("Invalid fine entered");
		}

		if (id == 0) {
			throw new ExceptionHandlerClass(
					"Invalid student ID entered, please enter a value greater than zero");
		}
		if (id < 0) {
			throw new ExceptionHandlerClass("Invalid student ID entered");
		}
		try {
			String query = "UPDATE UserStudent SET Fines=" + fine
					+ " WHERE id=" + id + ";";
			stmt.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return true;

	}

	public int ViewFine(int id)
			throws ExceptionHandlerClass, Exception {
		Conn_db conn = new Conn_db();
		stmt = (Statement) conn.connection("jdbc:mysql://127.0.0.1:3306/Agile");
		int result=0;
		if (id == 0) {
			throw new ExceptionHandlerClass(
					"Invalid student ID entered, please enter a value greater than zero");
		}
		if (id < 0) {
			throw new ExceptionHandlerClass("Invalid student ID entered");
		}
		try {
			String query = "Select Fines from UserStudent" + " WHERE id=" + id + ";";
			ResultSet rs=stmt.executeQuery(query);
			if(rs.next())
			{
				result=rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return result;

	}

}
