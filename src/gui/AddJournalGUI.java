package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;

import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import dao.DaoBook;
import dao.DaoJournal;
import testing.ExceptionHandlerClass;
import java.awt.Window.Type;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AddJournalGUI {

	private JFrame AddJournalWindow;
	private JTextField idTextBox;
	public static JTextField JournalNameTextBox;
	public static JTextField authorTextBox;
	public static JTextField libraryNumberTextBox;
	public static JTextField isbnTextBox;
	public static JTextField refNumberTextBox;
	public static JTextField releaseYearTextBox;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddJournalGUI window = new AddJournalGUI();
					window.AddJournalWindow.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AddJournalGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		AddJournalWindow = new JFrame();
		AddJournalWindow.setTitle("Add Journal");
		AddJournalWindow.setFont(new Font("Arial Rounded MT Bold", Font.PLAIN,
				12));
		AddJournalWindow.setBounds(100, 100, 300, 325);
		AddJournalWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		AddJournalWindow.getContentPane().setLayout(null);

		JLabel idLabel = new JLabel("Enter Id of Journal:");
		idLabel.setBounds(23, 11, 132, 14);
		AddJournalWindow.getContentPane().add(idLabel);

		idTextBox = new JTextField();
		idTextBox.setBounds(152, 8, 122, 20);
		AddJournalWindow.getContentPane().add(idTextBox);
		idTextBox.setColumns(10);

		JournalNameTextBox = new JTextField();
		JournalNameTextBox.setBounds(152, 63, 122, 20);
		AddJournalWindow.getContentPane().add(JournalNameTextBox);
		JournalNameTextBox.setColumns(10);

		authorTextBox = new JTextField();
		authorTextBox.setBounds(152, 94, 122, 20);
		AddJournalWindow.getContentPane().add(authorTextBox);
		authorTextBox.setColumns(10);

		libraryNumberTextBox = new JTextField();
		libraryNumberTextBox.setBounds(152, 125, 122, 20);
		AddJournalWindow.getContentPane().add(libraryNumberTextBox);
		libraryNumberTextBox.setColumns(10);

		isbnTextBox = new JTextField();
		isbnTextBox.setBounds(152, 156, 122, 20);
		AddJournalWindow.getContentPane().add(isbnTextBox);
		isbnTextBox.setColumns(10);

		refNumberTextBox = new JTextField();
		refNumberTextBox.setBounds(152, 187, 122, 20);
		AddJournalWindow.getContentPane().add(refNumberTextBox);
		refNumberTextBox.setColumns(10);

		releaseYearTextBox = new JTextField();
		releaseYearTextBox.setBounds(152, 218, 122, 20);
		AddJournalWindow.getContentPane().add(releaseYearTextBox);
		releaseYearTextBox.setColumns(10);

		JButton backButton = new JButton("Back");
		backButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AddJournalWindow.setVisible(false);
				AdminMenu menu = new AdminMenu();
				menu.main(null);
			}
		});
		backButton.setBounds(10, 247, 122, 23);
		AddJournalWindow.getContentPane().add(backButton);

		final JButton AddButton = new JButton("Add");
		AddButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int id = Integer.parseInt(idTextBox.getText());
				String name = JournalNameTextBox.getText();
				String author = authorTextBox.getText();
				int libraryNum = Integer.parseInt(libraryNumberTextBox
						.getText());
				int isbn = Integer.parseInt(isbnTextBox.getText());
				int refNum = Integer.parseInt(refNumberTextBox.getText());
				int year = Integer.parseInt(releaseYearTextBox.getText());
				try {
					DaoJournal.addJournal(id, name, author, libraryNum, isbn,
							refNum, year);
					JOptionPane.showMessageDialog(AddButton, "Add Successful");
				} catch (ExceptionHandlerClass e) {
					JOptionPane.showMessageDialog(AddButton, e);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		AddButton.setBounds(152, 249, 122, 23);
		AddJournalWindow.getContentPane().add(AddButton);

		JLabel bookNameLabel = new JLabel("Book Name:");
		bookNameLabel.setBounds(23, 66, 109, 14);
		AddJournalWindow.getContentPane().add(bookNameLabel);

		JLabel authorLabel = new JLabel("Author:");
		authorLabel.setBounds(23, 97, 109, 14);
		AddJournalWindow.getContentPane().add(authorLabel);

		JLabel libraryNumberLabel = new JLabel("Library Number:");
		libraryNumberLabel.setBounds(23, 128, 109, 14);
		AddJournalWindow.getContentPane().add(libraryNumberLabel);

		JLabel isbnLabel = new JLabel("ISBN Number:");
		isbnLabel.setBounds(23, 159, 109, 14);
		AddJournalWindow.getContentPane().add(isbnLabel);

		JLabel lblPleaseEnterUpdated = new JLabel(
				"Please Enter Journal Details");
		lblPleaseEnterUpdated.setBounds(67, 38, 180, 14);
		AddJournalWindow.getContentPane().add(lblPleaseEnterUpdated);

		JLabel refNumberLabel = new JLabel("Ref Number:");
		refNumberLabel.setBounds(23, 190, 109, 14);
		AddJournalWindow.getContentPane().add(refNumberLabel);

		JLabel releaseYearLabel = new JLabel("Release Year:");
		releaseYearLabel.setBounds(23, 221, 109, 14);
		AddJournalWindow.getContentPane().add(releaseYearLabel);
	}
}