package classFolder;

public class Student {
	private String username;
	private double fines;
	private double funds;

	public Student() {

	}

	public Student(String u, double fi, double fu) {
		this.username = u;
		this.fines = fi;
		this.funds = fu;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public double getFines() {
		return fines;
	}

	public void setFines(double fines) {
		this.fines = fines;
	}

	public double getFunds() {
		return funds;
	}

	public void setFunds(double funds) {
		this.funds = funds;
	}

}
