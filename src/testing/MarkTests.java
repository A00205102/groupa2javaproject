package testing;

import dao.DaoBook;
import static org.junit.Assert.*;
import ConnectDatabase.DaoviewReq;
import dao.DaoBook;
import dao.DaoJournal;
//import dao.DaoLogin;
import dao.Register_db;
import dao.Login_db;
import ConnectDatabase.Conn_db;
import ConnectDatabase.QueryTableModel;
import junit.framework.TestCase;
import testing.ExceptionHandlerClass;

public class MarkTests extends TestCase {

	// Test Number : 1
	// Test Objective: Test for all textbox's filled
	// Input(s): all fields
	// Expected output(s): true

	public void test1() {
		DaoBook testObject = new DaoBook();
		try {
			assertEquals(true,
					testObject.addBook("Agile", "Mark", 454, 54856, 49, 2017));

		} catch (ExceptionHandlerClass e) {
			e.getMessage();
		}
	}

	// //Test number: 2
	// //Test objective: Test input name greater than 20 characters
	// //Input(s): Book name = ""
	// //Expected Output(s): Exception
	// //Added by: Mark

	public void test2() {
		DaoBook testObject = new DaoBook();
		try {
			testObject.addBook("jjjjjjjjjjjjjjjjjjjji", "", 0, 0, 0, 0);
			// fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid Book name entered", e.getMessage());
		}
	}

	// //Test number: 3
	// //Test objective: Test input name less than 20 characters
	// //Input(s): Book name = ""
	// //Expected Output(s): Boolean = Valid
	// //Added by: Mark

	public void test3() {
		DaoBook testObject = new DaoBook();
		try {
			assertEquals(true, testObject.addBook("Mark", "", 0, 0, 0, 0));
			// fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			e.getMessage();
		}
	}

	// //Test number: 4
	// //Test objective: Test input author greater than 20 characters
	// //Input(s): Author name = ""
	// //Expected Output(s): Boolean = Invalid
	// //Added by: Mark

	public void test4() {
		DaoBook testObject = new DaoBook();
		try {
			testObject.addBook("", "jjjjjjjjjjjjjjjjjjjji", 0, 0, 0, 0);
			// fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid Book name entered", e.getMessage());
		}
	}

	// //Test number: 5
	// //Test objective: Test input author less than 20 characters
	// //Input(s): Author name = ""
	// //Expected Output(s): true
	// //Added by: Mark

	public void test5() {
		DaoBook testObject = new DaoBook();
		try {
			assertEquals(true, testObject.addBook("", "Mark", 0, 0, 0, 0));

		} catch (ExceptionHandlerClass e) {
			e.getMessage();
		}
	}

	// Test Number : 6
	// Test Objective: Test for all textbox's filled
	// Input(s): all fields
	// Expected output(s): true
	// //Added by: Mark

	public void test6() {
		DaoBook testObject = new DaoBook();
		try {
			assertEquals(true,
					testObject.addBook("Agile", "Mark", 454, 54856, 49, 2017));

		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid Book name entered", e.getMessage());
		}
	}

	// Test Number:7;
	// Test Objective: Test for add title
	// Input(s): title= !
	// Expected Output(s)= Exception

	public void test7() {

		DaoBook testObject = new DaoBook();
		try {

			testObject.addBook("!", "", 0, 0, 0, 0);

		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid Book name entered", e.getMessage());
		}

	}

	// Test Number:8;
	// Test Objective: Test for add author
	// Input(s): title= !
	// Expected Output(s)= Exception

	public void test8() {

		DaoBook testObject = new DaoBook();
		try {

			testObject.addBook("", "!", 0, 0, 0, 0);

		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid author name entered", e.getMessage());
		}

	}

	// Test Number:9;
	// Test Objective: Test for add library number
	// Input(s): 123456
	// Expected Output(s)= true

	public void test9() {

		DaoBook testObject = new DaoBook();

		try {

			assertEquals(true, testObject.addBook("", "", 123456, 0, 0, 0));

		} catch (ExceptionHandlerClass e) {
			e.getMessage();
		}
	}

	// Test Number:10;
	// Test Objective: Test for add library number
	// Input(s): 99999999
	// Expected Output(s)= Exception

	public void test10() {

		DaoBook testObject = new DaoBook();

		try {

			testObject.addBook("", "", 99999999, 0, 0, 0);

		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid author name entered", e.getMessage());
		}

	}

	// Test Number:11;
	// Test Objective: Test for add library number
	// Input(s): -1
	// Expected Output(s)= true

	public void test11() {

		DaoBook testObject = new DaoBook();

		try {

			testObject.addBook("", "", -1, 0, 0, 0);

		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid author name entered", e.getMessage());
		}

	}

	// Test Number:12;
	// Test Objective: Test for add ISBN
	// Input(s): 123456
	// Expected Output(s)= true

	public void test12() {

		DaoBook testObject = new DaoBook();

		try {

			assertEquals(true, testObject.addBook("", "", 0, 123456, 0, 0));

		} catch (ExceptionHandlerClass e) {
			e.getMessage();
		}
	}

	// Test Number:13;
	// Test Objective: Test for add ISBN
	// Input(s): 99999999
	// Expected Output(s)= Exception

	public void test13() {

		DaoBook testObject = new DaoBook();

		try {

			testObject.addBook("", "", 0, 99999999, 0, 0);

		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid author name entered", e.getMessage());
		}

	}

	// Test Number:14;
	// Test Objective: Test for add ISBN
	// Input(s): -1
	// Expected Output(s)= Exception

	public void test14() {

		DaoBook testObject = new DaoBook();

		try {

			testObject.addBook("", "", 0, -1, 0, 0);

		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid author name entered", e.getMessage());
		}

	}

	// Test Number:15;
	// Test Objective: Test for add Reference Number
	// Input(s): 123456
	// Expected Output(s)= true

	public void test15() {

		DaoBook testObject = new DaoBook();

		try {

			assertEquals(true, testObject.addBook("", "", 0, 0, 123456, 0));

		} catch (ExceptionHandlerClass e) {
			e.getMessage();
		}
	}

	// Test Number:16;
	// Test Objective: Test for add Reference Number
	// Input(s): 99999999
	// Expected Output(s)= Exception

	public void test16() {

		DaoBook testObject = new DaoBook();

		try {

			testObject.addBook("", "", 0, 0, 99999999, 0);

		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid author name entered", e.getMessage());
		}

	}

	// Test Number:17;
	// Test Objective: Test for add Reference Number
	// Input(s): -1
	// Expected Output(s)= Exception

	public void test17() {

		DaoBook testObject = new DaoBook();

		try {

			testObject.addBook("", "", 0, 0, -1, 0);

		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid author name entered", e.getMessage());
		}

	}

	// Test Number:18;
	// Test Objective: Test for add Release Year
	// Input(s): 2014
	// Expected Output(s)= true

	public void test18() {

		DaoBook testObject = new DaoBook();

		try {

			assertEquals(true, testObject.addBook("", "", 0, 0, 0, 2014));

		} catch (ExceptionHandlerClass e) {
			e.getMessage();
		}
	}

	// Test Number:19;
	// Test Objective: Test for add Release Year
	// Input(s): 99999
	// Expected Output(s)= Exception

	public void test19() {

		DaoBook testObject = new DaoBook();

		try {

			testObject.addBook("", "", 0, 0, 0, 99999);

		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid author name entered", e.getMessage());
		}

	}

	// Test Number:20;
	// Test Objective: Test for add Release Year
	// Input(s): -0001
	// Expected Output(s)= Exception

	public void test20() {

		DaoBook testObject = new DaoBook();

		try {

			testObject.addBook("", "", 0, 0, 0, -0001);

		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid author name entered", e.getMessage());
		}

	}

}
