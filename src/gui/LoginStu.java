package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;

import dao.DaoStudent;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LoginStu extends DaoStudent {

	private JFrame frame;
	private JTextField username;
	private JTextField password;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginStu window = new LoginStu();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LoginStu() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		JLabel lblEnterYour = new JLabel("Enter your username");
		lblEnterYour.setBounds(36, 78, 149, 15);
		panel.add(lblEnterYour);

		JLabel lblEnterOurPassword = new JLabel("Enter your password");
		lblEnterOurPassword.setBounds(36, 106, 149, 15);
		panel.add(lblEnterOurPassword);

		username = new JTextField();
		username.setBounds(203, 73, 200, 26);
		panel.add(username);
		username.setColumns(10);

		password = new JTextField();
		password.setColumns(10);
		password.setBounds(203, 106, 200, 26);
		panel.add(password);

		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String user = username.getText();
				String pass = password.getText();
				boolean res = login(user, pass);
				if (res == true) {
					StudentMenu sm = new StudentMenu(user);
					sm.main(null);
					frame.setVisible(false);
				} else {
					username.setText("");
					password.setText("");
				}

			}
		});
		btnLogin.setBounds(68, 165, 117, 25);
		panel.add(btnLogin);

		JButton btnRegister = new JButton("Register");
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RegisterStu sr = new RegisterStu();
				sr.main(null);
				frame.setVisible(false);
			}
		});
		btnRegister.setBounds(247, 165, 117, 25);
		panel.add(btnRegister);
	}
}
