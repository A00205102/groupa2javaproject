package classFolder;

public class RequestFromDB {
	private int id;
	private String title;
	private String author;
	private int ISBN;
	private String email;

	public RequestFromDB(int id, String title, String author, int iSBN,
			String email) {
		super();
		this.id = id;
		this.title = title;
		this.author = author;
		ISBN = iSBN;
		this.email = email;
	}

	public RequestFromDB() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getAuthor() {
		return author;
	}

	public int getISBN() {
		return ISBN;
	}

	public String getEmail() {
		return email;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setISBN(int iSBN) {
		ISBN = iSBN;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
