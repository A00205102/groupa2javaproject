package testing;



import dao.DaoFine;
import junit.framework.TestCase;


public class ViewFineTest extends TestCase {
	// Test number: 1
		// Test objective: Test valid input
		// Input(s): student id = 1
		// Expected Output(s): Boolean = true
		// Added by: Thomas
		public void test1() throws Exception {
			DaoFine testObject = new DaoFine();
			try {
				assertEquals(50, testObject.ViewFine(1));
			} catch (ExceptionHandlerClass e) {
				fail("Should not reach here ... no exception expected");
			}
		}

	



// Test number: 2
// Test objective: Test invalid input
// Input(s): student id = 1
// Expected Output(s): Boolean = true
// Added by: Thomas
public void test2() throws Exception {
	DaoFine testObject = new DaoFine();
	try {
		 testObject.ViewFine(0);
		 fail("Should not get here .. Exception Expected");
	} catch (ExceptionHandlerClass e) {
		assertEquals("Invalid student ID entered, please enter a value greater than zero", e.getMessage());
	}
}



// Test number: 3
// Test objective: Test invalid input
// Input(s): student id = 1
// Expected Output(s): Boolean = true
// Added by: Thomas
public void test3() throws Exception {
	DaoFine testObject = new DaoFine();
	try {
		 testObject.ViewFine(-1);
		 fail("Should not get here .. Exception Expected");
	} catch (ExceptionHandlerClass e) {
		assertEquals("Invalid student ID entered", e.getMessage());
	}
}



// Test number: 4
// Test objective: Test invalid input
// Input(s): student id = 1
// Expected Output(s): Boolean = true
// Added by: Thomas
public void test4() throws Exception {
	DaoFine testObject = new DaoFine();
	try {
		 testObject.ViewFine(Integer.MIN_VALUE);
		 fail("Should not get here .. Exception Expected");
	} catch (ExceptionHandlerClass e) {
		assertEquals("Invalid student ID entered", e.getMessage());
	}
}



// Test number: 5
// Test objective: Test invalid input
// Input(s): student id = 1
// Expected Output(s): Boolean = true
// Added by: Thomas
public void test5() throws Exception {
	DaoFine testObject = new DaoFine();
	try {
		 testObject.ViewFine(Integer.MAX_VALUE);
	} catch (ExceptionHandlerClass e) {
		assertEquals("Invalid student ID entered", e.getMessage());
	}
  }


//Test number: 6
//Test objective: Test invalid input
//Input(s): student id = 1
//Expected Output(s): Boolean = true
//Added by: Thomas
public void test6() throws Exception {
	DaoFine testObject = new DaoFine();
	try {
		 testObject.ViewFine(Integer.MAX_VALUE+1);
	} catch (ExceptionHandlerClass e) {
		assertEquals("Invalid student ID entered", e.getMessage());
	}
}

//Test number: 7
//Test objective: Test invalid input
//Input(s): student id = 1
//Expected Output(s): Boolean = true
//Added by: Thomas
public void test7() throws Exception {
	DaoFine testObject = new DaoFine();
	try {
		 testObject.ViewFine(Integer.MAX_VALUE-1);
	} catch (ExceptionHandlerClass e) {
		assertEquals("Invalid student ID entered", e.getMessage());
	}
}

//Test number: 8
//Test objective: Test invalid input
//Input(s): student id = 1
//Expected Output(s): Boolean = true
//Added by: Thomas
public void test8() throws Exception {
	DaoFine testObject = new DaoFine();
	try {
		 testObject.ViewFine(Integer.MIN_VALUE+1);
		 fail("Should not get here .. Exception Expected");
	} catch (ExceptionHandlerClass e) {
		assertEquals("Invalid student ID entered", e.getMessage());
	}
}	
	//Test number: 9
	//Test objective: Test invalid input
	//Input(s): student id = 1
	//Expected Output(s): Boolean = true
	//Added by: Thomas
	public void test9() throws Exception {
		DaoFine testObject = new DaoFine();
		try {
			 testObject.ViewFine(Integer.MIN_VALUE-1);
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid student ID entered", e.getMessage());
		}
	}
}

