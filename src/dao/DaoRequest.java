/*
 * https://www.google.com/settings/security/lesssecureapps
 */

package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

import javax.mail.*;
import javax.mail.internet.*;

import testing.ExceptionHandlerClass;

import classFolder.RequestFromDB;
import ConnectDatabase.Conn_db;

public class DaoRequest extends Conn_db {
	private static Connection con = null;
	private static Statement stmt = null;
	private static ResultSet rs = null;
	static RequestFromDB test1 = new RequestFromDB();

	public static RequestFromDB bookRequestFromDb(int id)
			throws ClassNotFoundException, SQLException, ExceptionHandlerClass {
		if (id == 0) {
			throw new ExceptionHandlerClass("Invalid Book ID entered");
		}
		if (id < 0) {
			throw new ExceptionHandlerClass("Invalid Book ID entered");
		} else {
			Class.forName("com.mysql.jdbc.Driver");

			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3307/agile", "root", "admin");

			PreparedStatement psmt = con.prepareStatement("SELECT * FROM req_Book where id=" + id);

			ResultSet rs = psmt.executeQuery();

			while (rs.next()) {
				RequestFromDB test = new RequestFromDB(rs.getInt("id"), rs.getString("title"), rs.getString("author"),
						rs.getInt("ISBN"), rs.getString("email"));
				test1 = test;
			}
			return test1;
		}
	}

	public static RequestFromDB journalRequestFromDb(int id)
			throws ClassNotFoundException, SQLException, ExceptionHandlerClass {
		if (id <= 0) {
			throw new ExceptionHandlerClass("Invalid Book ID entered");
		} else {
			Class.forName("com.mysql.jdbc.Driver");

			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3307/agile", "root", "admin");

			PreparedStatement psmt = con.prepareStatement("SELECT * FROM req_Journal WHERE id=" + id);

			rs = psmt.executeQuery();

			while (rs.next()) {
				RequestFromDB test = new RequestFromDB(rs.getInt("id"), rs.getString("title"), rs.getString("author"),
						rs.getInt("ISBN"), rs.getString("email"));
				test1 = test;
			}
			return test1;
		}
	}

	public static void sendEmailNotificationForRequest(RequestFromDB b) {
		try {
			String host = "smtp.gmail.com";
			String user = "collegelibrarysystem@gmail.com";
			String pass = "collegeLibrarySystem123";
			String to = b.getEmail();
			String from = "collegelibrarysystem@gmail.com";
			String subject = "Book/Journal Request";
			String messageText = "Hi, Just to let you know, the book/journal you have requested is now back in stock. Details are listed below\n\nBook/Journal ID: "
					+ b.getId() + "\nTitle: " + b.getTitle() + "\nAuthor: " + b.getAuthor() + "\nISBN: " + b.getISBN();
			boolean sessionDebug = false;

			Properties props = System.getProperties();
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", host);
			props.put("mail.stmp.port", "587");
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.required", "true");

			java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
			Session mailSession = Session.getDefaultInstance(props, null);
			Message msg = new MimeMessage(mailSession);
			msg.setFrom(new InternetAddress(from));
			InternetAddress address = new InternetAddress(to);
			msg.setRecipient(Message.RecipientType.TO, address);
			msg.setSubject(subject);
			msg.setText(messageText);

			Transport transport = mailSession.getTransport("smtp");
			transport.connect(host, user, pass);
			transport.sendMessage(msg, msg.getAllRecipients());
			transport.close();
			System.out.println("Send successful");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void sendEmailNotificationForExpiry(String title, String expiry, String email) {
		try {
			String host = "smtp.gmail.com";
			String user = "collegelibrarysystem@gmail.com";
			String pass = "collegeLibrarySystem123";
			String to = email;
			String from = "collegelibrarysystem@gmail.com";
			String subject = "Book/Journal Expiry";
			String messageText = "Hi, Just to let you know, the book/journal you have borrowed, '" + title
					+ "' is to be returned on " + expiry;
			boolean sessionDebug = false;

			Properties props = System.getProperties();
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", host);
			props.put("mail.stmp.port", "587");
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.required", "true");

			java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
			Session mailSession = Session.getDefaultInstance(props, null);
			Message msg = new MimeMessage(mailSession);
			msg.setFrom(new InternetAddress(from));
			InternetAddress address = new InternetAddress(to);
			msg.setRecipient(Message.RecipientType.TO, address);
			msg.setSubject(subject);
			msg.setText(messageText);

			Transport transport = mailSession.getTransport("smtp");
			transport.connect(host, user, pass);
			transport.sendMessage(msg, msg.getAllRecipients());
			transport.close();
			System.out.println("Send successful");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
