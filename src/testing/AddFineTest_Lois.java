package testing;

import dao.DaoBook;
import dao.DaoFine;
import dao.Register_db;
import junit.framework.TestCase;

public class AddFineTest_Lois extends TestCase {
	// Test number: 1
	// Test objective: Test valid input
	// Input(s): student id = 1,name=stu1,fine=1.8
	// Expected Output(s): Boolean = true
	// Added by: Lois
	public void test001()  {
		DaoFine testObject = new DaoFine();
		try {
			assertTrue(testObject.AddFine(1, "stu1", 1.8));
		} catch (Exception e) {
			e.printStackTrace();
			//fail("Should not reach here ... no exception expected");
		}
	}
	// Test number: 2
	// Test objective: Test invalid input
	// Input(s): student id = 2,name=stu1,fine=1.8
	// Expected Output(s): Boolean = true
	// Added by: Lois
	public void test002()  {
		DaoFine testObject = new DaoFine();
		try {
			assertFalse(testObject.AddFine(2, "stu1", 1.8));
		} catch (Exception e) {
			e.printStackTrace();
			//fail("Should not reach here ... no exception expected");
		}
	}
	// Test number: 3
	// Test objective: Test invalid input
	// Input(s): student id = 1,name=stu2,fine=1.8
	// Expected Output(s): Boolean = true
	// Added by: Lois
	public void test003()  {
		DaoFine testObject = new DaoFine();
		try {
			assertFalse(testObject.AddFine(1, "stu2", 1.8));
		} catch (Exception e) {
			e.printStackTrace();
			//fail("Should not reach here ... no exception expected");
		}
	}
	
	// Test number: 4
	// Test objective: Test invalid input
	// Input(s): student id = 1,name=stu2,fine=-1
	// Expected Output(s): Boolean = true
	// Added by: Lois
	public void test004()  {
		DaoFine testObject = new DaoFine();
		try {
			assertFalse(testObject.AddFine(1, "stu1", -1));
		} catch (Exception e) {
			e.printStackTrace();
			//fail("Should not reach here ... no exception expected");
		}
	}
	// Test number:5
	// Test objective: Test invalid input
	// Input(s): student id = -1,name=stu1,fine=-1
	// Expected Output(s): Boolean = true
	// Added by: Lois
	public void test005()  {
		DaoFine testObject = new DaoFine();
		try {
			assertFalse(testObject.AddFine(-1, "stu1", -1));
		} catch (Exception e) {
			e.printStackTrace();
			//fail("Should not reach here ... no exception expected");
		}
	}
	// Test number:6
	// Test objective: Test invalid input
	// Input(s): student id = -1,name= ,fine=-1
	// Expected Output(s): Boolean = true
	// Added by: Lois
	public void test006()  {
		DaoFine testObject = new DaoFine();
		try {
			assertFalse(testObject.AddFine(1, " ", -1));
		} catch (Exception e) {
			e.printStackTrace();
			//fail("Should not reach here ... no exception expected");
		}
	}
	// Test number:7
	// Test objective: Test invalid input
	// Input(s): student id = -1,name= ,fine=-1
	// Expected Output(s): Boolean = true
	// Added by: Lois
	public void test007()  {
		DaoFine testObject = new DaoFine();
		try {
			assertFalse(testObject.AddFine(-1, " ", -1));
		} catch (Exception e) {
			e.printStackTrace();
			//fail("Should not reach here ... no exception expected");
		}
	}
	// Test number:8
	// Test objective: Test invalid input
	// Input(s): student id = -1,name= ,fine=1
	// Expected Output(s): Boolean = true
	// Added by: Lois
	public void test008()  {
		DaoFine testObject = new DaoFine();
		try {
			assertFalse(testObject.AddFine(-1, " ", 1));
		} catch (Exception e) {
			e.printStackTrace();
			//fail("Should not reach here ... no exception expected");
		}
	}
	// Test number:9
	// Test objective: Test invalid input
	// Input(s): student id = -1,name= ,fine=-1
	// Expected Output(s): Boolean = true
	// Added by: Lois
	public void test009()  {
		DaoFine testObject = new DaoFine();
		try {
			assertFalse(testObject.AddFine(1, " ", 1));
		} catch (Exception e) {
			e.printStackTrace();
			//fail("Should not reach here ... no exception expected");
		}
	}
	
	// Test number:10
	// Test objective: Test invalid input
	// Input(s): student id = -1,name=stu1,fine=1
	// Expected Output(s): Boolean = true
	// Added by: Lois
	public void test0010()  {
		DaoFine testObject = new DaoFine();
		try {
			assertFalse(testObject.AddFine(-1, "stu1", 1));
		} catch (Exception e) {
			e.printStackTrace();
			//fail("Should not reach here ... no exception expected");
		}
	}
	
	
	// Test number: 11
	// Test objective: Test valid input
	// Input(s): student id = 1,name=stu1,fine=0
	// Expected Output(s): Boolean = true
	// Added by: Lois
	public void test0011()  {
		DaoFine testObject = new DaoFine();
		try {
			assertTrue(testObject.AddFine(1, "stu1", 0));
		} catch (Exception e) {
			e.printStackTrace();
			//fail("Should not reach here ... no exception expected");
		}
	}
	// Test number:12
	// Test objective: Test invalid input
	// Input(s): student id = 2,name=stu1,fine=0
	// Expected Output(s): Boolean = true
	// Added by: Lois
	public void test0012()  {
		DaoFine testObject = new DaoFine();
		try {
			assertFalse(testObject.AddFine(2, "stu1", 0));
		} catch (Exception e) {
			e.printStackTrace();
			//fail("Should not reach here ... no exception expected");
		}
	}
	// Test number: 13
	// Test objective: Test invalid input
	// Input(s): student id = 1,name=stu2,fine=0
	// Expected Output(s): Boolean = true
	// Added by: Lois
	public void test0013()  {
		DaoFine testObject = new DaoFine();
		try {
			assertFalse(testObject.AddFine(1, "stu2", 0));
		} catch (Exception e) {
			e.printStackTrace();
			//fail("Should not reach here ... no exception expected");
		}
	}
	
	// Test number: 14
	// Test objective: Test invalid input
	// Input(s): student id = 1,name=stu2,fine=0
	// Expected Output(s): Boolean = true
	// Added by: Lois
	public void test0014()  {
		DaoFine testObject = new DaoFine();
		try {
			assertFalse(testObject.AddFine(1, "stu1", 0));
		} catch (Exception e) {
			e.printStackTrace();
			//fail("Should not reach here ... no exception expected");
		}
	}
	// Test number:15
	// Test objective: Test invalid input
	// Input(s): student id = -1,name=stu1,fine=-1
	// Expected Output(s): Boolean = true
	// Added by: Lois
	public void test0015()  {
		DaoFine testObject = new DaoFine();
		try {
			assertFalse(testObject.AddFine(-1, "stu1", 0));
		} catch (Exception e) {
			e.printStackTrace();
			//fail("Should not reach here ... no exception expected");
		}
	}
	// Test number:16
	// Test objective: Test invalid input
	// Input(s): student id = -1,name= ,fine=-1
	// Expected Output(s): Boolean = true
	// Added by: Lois
	public void test0016()  {
		DaoFine testObject = new DaoFine();
		try {
			assertFalse(testObject.AddFine(1, " ", 0));
		} catch (Exception e) {
			e.printStackTrace();
			//fail("Should not reach here ... no exception expected");
		}
	}
	// Test number:17
	// Test objective: Test invalid input
	// Input(s): student id = -1,name= ,fine=0
	// Expected Output(s): Boolean = true
	// Added by: Lois
	public void test0017()  {
		DaoFine testObject = new DaoFine();
		try {
			assertFalse(testObject.AddFine(-1, " ", 0));
		} catch (Exception e) {
			e.printStackTrace();
			//fail("Should not reach here ... no exception expected");
		}
	}
	// Test number:18
	// Test objective: Test invalid input
	// Input(s): student id = -1,name= ,fine=0
	// Expected Output(s): Boolean = true
	// Added by: Lois
	public void test0018()  {
		DaoFine testObject = new DaoFine();
		try {
			assertFalse(testObject.AddFine(-1, " ", 0));
		} catch (Exception e) {
			e.printStackTrace();
			//fail("Should not reach here ... no exception expected");
		}
	}
	// Test number:19
	// Test objective: Test invalid input
	// Input(s): student id = -1,name= ,fine=0
	// Expected Output(s): Boolean = true
	// Added by: Lois
	public void test0019()  {
		DaoFine testObject = new DaoFine();
		try {
			assertFalse(testObject.AddFine(1, " ", 0));
		} catch (Exception e) {
			e.printStackTrace();
			//fail("Should not reach here ... no exception expected");
		}
	}
	
	// Test number:20
	// Test objective: Test invalid input
	// Input(s): student id = -1,name=stu1,fine=0
	// Expected Output(s): Boolean = true
	// Added by: Lois
	public void test0020()  {
		DaoFine testObject = new DaoFine();
		try {
			assertFalse(testObject.AddFine(-1, "stu1", 0));
		} catch (Exception e) {
			e.printStackTrace();
			//fail("Should not reach here ... no exception expected");
		}
	}
	
	
	}

