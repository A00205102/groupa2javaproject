package testing;

import classFolder.RequestFromDB;
import dao.DaoBook;
import dao.DaoRequest;
import junit.framework.TestCase;

public class ShaneTest extends TestCase {
	// Test number: 1
	// Test objective: Test valid input
	// Input(s): Book id = 1
	// Expected Output(s): Boolean = true
	// Added by: Shane
	public void test1() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			assertEquals(true,
					testObject.updateBook(1, "example", "example", 1, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 2
	// Test objective: Test valid input
	// Input(s): Book id = MAX_INT
	// Expected Output(s): Boolean = true
	// Added by: Shane
	public void test2() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			assertEquals(true, testObject.updateBook(Integer.MAX_VALUE,
					"example", "example", 1, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 3
	// Test objective: Test invalid input
	// Input(s): Book id = 0
	// Expected Output(s): Boolean = false
	// Added by: Shane
	public void test3() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			testObject.updateBook(0, "example", "example", 1, 1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals(
					"Invalid Book ID entered, please enter a value greater than zero",
					e.getMessage());
		}
	}

	// Test number: 4
	// Test objective: Test invalid input
	// Input(s): Book id = -1
	// Expected Output(s): Boolean = false
	// Added by: Shane
	public void test4() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			testObject.updateBook(-1, "example", "example", 1, 1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid Book ID entered", e.getMessage());
		}
	}

	// Test number: 5
	// Test objective: Test invalid input
	// Input(s): Book id = MIN_INT
	// Expected Output(s): Boolean = false
	// Added by: Shane
	public void test5() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			testObject.updateBook(Integer.MIN_VALUE, "example", "example", 1,
					1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid Book ID entered", e.getMessage());
		}
	}

	// -------------------------------------------------

	// Test number: 6
	// Test objective: Test valid input
	// Input(s): Book name = empty string
	// Expected Output(s): Boolean = false
	// Added by: Shane
	public void test6() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			testObject.updateBook(1, "", "example", 1, 1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Please enter a book name", e.getMessage());
		}
	}

	// Test number: 7
	// Test objective: Test valid input
	// Input(s): Book name = 1 character
	// Expected Output(s): Boolean = true
	// Added by: Shane
	public void test7() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			assertEquals(true,
					testObject.updateBook(1, "a", "example", 1, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 8
	// Test objective: Test valid input
	// Input(s): Book name = 20 characters
	// Expected Output(s): Boolean = true
	// Added by: Shane
	public void test8() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			assertEquals(true, testObject.updateBook(1, "aaaaaaaaaaaaaaaaaaaa",
					"example", 1, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 9
	// Test objective: Test invalid input
	// Input(s): Book name = greater than 20 characters
	// Expected Output(s): Boolean = false
	// Added by: Shane
	public void test9() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			testObject.updateBook(1, "aaaaaaaaaaaaaaaaaaaaa", "example", 1, 1,
					1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals(
					"Maximum character amount exceeded for book name (20)",
					e.getMessage());
		}
	}

	// --------------------------------------------------

	// Test number: 10
	// Test objective: Test invalid input
	// Input(s): Book name = empty string
	// Expected Output(s): Boolean = false
	// Added by: Shane
	public void test10() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			testObject.updateBook(1, "example", "", 1, 1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Please enter authors name", e.getMessage());
		}
	}

	// Test number: 11
	// Test objective: Test valid input
	// Input(s): Book name = 1 character
	// Expected Output(s): Boolean = true
	// Added by: Shane
	public void test11() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			assertEquals(true,
					testObject.updateBook(1, "example", "a", 1, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 12
	// Test objective: Test valid input
	// Input(s): Book name = 20 characters
	// Expected Output(s): Boolean = true
	// Added by: Shane
	public void test12() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			assertEquals(true, testObject.updateBook(1, "example",
					"aaaaaaaaaaaaaaaaaaaa", 1, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 13
	// Test objective: Test invalid input
	// Input(s): Book name = greater than 20 characters
	// Expected Output(s): Boolean = false
	// Added by: Shane
	public void test13() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			testObject.updateBook(1, "example", "aaaaaaaaaaaaaaaaaaaaa", 1, 1,
					1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals(
					"Maximum character amount exceeded for author name (20)",
					e.getMessage());
		}
	}

	// --------------------------------------------
	// Test number: 14
	// Test objective: Test valid input
	// Input(s): library number = 1
	// Expected Output(s): Boolean = true
	// Added by: Shane
	public void test14() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			assertEquals(true,
					testObject.updateBook(1, "example", "example", 1, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 15
	// Test objective: Test valid input
	// Input(s): library number = MAX_INT
	// Expected Output(s): Boolean = true
	// Added by: Shane
	public void test15() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			assertEquals(true, testObject.updateBook(1, "example", "example",
					Integer.MAX_VALUE, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 16
	// Test objective: Test invalid input
	// Input(s): library number = 0
	// Expected Output(s): Boolean = false
	// Added by: Shane
	public void test16() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			testObject.updateBook(1, "example", "example", 0, 1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals(
					"Invalid library number entered, please enter a value greater than zero",
					e.getMessage());
		}
	}

	// Test number: 17
	// Test objective: Test invalid input
	// Input(s): library number = -1
	// Expected Output(s): Boolean = false
	// Added by: Shane
	public void test17() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			testObject.updateBook(1, "example", "example", -1, 1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid Library number entered", e.getMessage());
		}
	}

	// Test number: 18
	// Test objective: Test invalid input
	// Input(s): library njmber = MIN_INT
	// Expected Output(s): Boolean = false
	// Added by: Shane
	public void test18() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			testObject.updateBook(1, "example", "example", Integer.MIN_VALUE,
					1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid Library number entered", e.getMessage());
		}
	}

	// --------------------------------------------
	// Test number: 19
	// Test objective: Test valid input
	// Input(s): isbn number = 1
	// Expected Output(s): Boolean = true
	// Added by: Shane
	public void test19() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			assertEquals(true,
					testObject.updateBook(1, "example", "example", 1, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 20
	// Test objective: Test valid input
	// Input(s): isbn number = MAX_INT
	// Expected Output(s): Boolean = true
	// Added by: Shane
	public void test20() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			assertEquals(true, testObject.updateBook(1, "example", "example",
					1, Integer.MAX_VALUE, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 21
	// Test objective: Test invalid input
	// Input(s): isbn number = 0
	// Expected Output(s): Boolean = false
	// Added by: Shane
	public void test21() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			testObject.updateBook(1, "example", "example", 1, 0, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals(
					"Invalid ISBN number entered, please enter a value greater than zero",
					e.getMessage());
		}
	}

	// Test number: 22
	// Test objective: Test invalid input
	// Input(s): isbn number = -1
	// Expected Output(s): Boolean = false
	// Added by: Shane
	public void test22() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			testObject.updateBook(1, "example", "example", 1, -1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid ISBN number entered", e.getMessage());
		}
	}

	// Test number: 23
	// Test objective: Test invalid input
	// Input(s): isbn njmber = MIN_INT
	// Expected Output(s): Boolean = false
	// Added by: Shane
	public void test23() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			testObject.updateBook(1, "example", "example", 1,
					Integer.MIN_VALUE, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid ISBN number entered", e.getMessage());
		}
	}

	// --------------------------------------------
	// Test number: 24
	// Test objective: Test valid input
	// Input(s): ref number = 1
	// Expected Output(s): Boolean = true
	// Added by: Shane
	public void test24() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			assertEquals(true,
					testObject.updateBook(1, "example", "example", 1, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 25
	// Test objective: Test valid input
	// Input(s): ref number = MAX_INT
	// Expected Output(s): Boolean = true
	// Added by: Shane
	public void test25() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			assertEquals(true, testObject.updateBook(1, "example", "example",
					1, 1, Integer.MAX_VALUE, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 26
	// Test objective: Test invalid input
	// Input(s): ref number = 0
	// Expected Output(s): Boolean = false
	// Added by: Shane
	public void test26() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			testObject.updateBook(1, "example", "example", 1, 1, 0, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals(
					"Invalid reference number entered, please enter a value greater than zero",
					e.getMessage());
		}
	}

	// Test number: 27
	// Test objective: Test invalid input
	// Input(s): ref number = -1
	// Expected Output(s): Boolean = false
	// Added by: Shane
	public void test27() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			testObject.updateBook(1, "example", "example", 1, 1, -1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid reference Number entered", e.getMessage());
		}
	}

	// Test number: 28
	// Test objective: Test invalid input
	// Input(s): ref njmber = MIN_INT
	// Expected Output(s): Boolean = false
	// Added by: Shane
	public void test28() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			testObject.updateBook(1, "example", "example", 1, 1,
					Integer.MIN_VALUE, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid reference Number entered", e.getMessage());
		}
	}

	// --------------------------------------------
	// Test number: 29
	// Test objective: Test valid input
	// Input(s): year = 1
	// Expected Output(s): Boolean = true
	// Added by: Shane
	public void test29() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			assertEquals(true,
					testObject.updateBook(1, "example", "example", 1, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 30
	// Test objective: Test valid input
	// Input(s): year = MAX_INT
	// Expected Output(s): Boolean = true
	// Added by: Shane
	public void test30() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			assertEquals(true, testObject.updateBook(1, "example", "example",
					1, 1, 1, Integer.MAX_VALUE));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 31
	// Test objective: Test invalid input
	// Input(s): year = 0
	// Expected Output(s): Boolean = false
	// Added by: Shane
	public void test31() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			testObject.updateBook(1, "example", "example", 1, 1, 1, 0);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals(
					"Invalid year entered, please enter a value greater than zero",
					e.getMessage());
		}
	}

	// Test number: 32
	// Test objective: Test invalid input
	// Input(s): year = -1
	// Expected Output(s): Boolean = false
	// Added by: Shane
	public void test32() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			testObject.updateBook(1, "example", "example", 1, 1, 1, -1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid year entered", e.getMessage());
		}
	}

	// Test number: 33
	// Test objective: Test invalid input
	// Input(s): year = MIN_INT
	// Expected Output(s): Boolean = false
	// Added by: Shane
	public void test33() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			testObject.updateBook(1, "example", "example", 1, 1, 1,
					Integer.MIN_VALUE);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid year entered", e.getMessage());
		}
	}

	// ///////////////////////////////////////////////////////////////

	// Test number: 34
	// Test objective: Test valid input
	// Input(s): Book id = 1
	// Expected Output(s): RequestFromDB object
	// Added by: Shane
	public void test34() throws Exception {
		DaoRequest testObject = new DaoRequest();
		try {
			testObject.bookRequestFromDb(1);
			assertNotNull(testObject);
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 35
	// Test objective: Test valid input
	// Input(s): Book id = max int
	// Expected Output(s): RequestFromDB object
	// Added by: Shane
	public void test35() throws Exception {
		DaoRequest testObject = new DaoRequest();
		try {
			testObject.bookRequestFromDb(Integer.MAX_VALUE);
			assertNotNull(testObject);
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 36
	// Test objective: Test invalid input
	// Input(s): Book id = 0
	// Expected Output(s): -
	// Added by: Shane
	public void test36() throws Exception {
		DaoRequest testObject = new DaoRequest();
		try {
			testObject.bookRequestFromDb(0);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid Book ID entered", e.getMessage());
		}
	}

	// Test number: 37
	// Test objective: Test invalid input
	// Input(s): Book id = -1
	// Expected Output(s): -
	// Added by: Shane
	public void test37() throws Exception {
		DaoRequest testObject = new DaoRequest();
		try {
			testObject.bookRequestFromDb(-1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid Book ID entered", e.getMessage());
		}
	}

	// Test number: 38
	// Test objective: Test invalid input
	// Input(s): Book id = min int
	// Expected Output(s): -
	// Added by: Shane
	public void test38() throws Exception {
		DaoRequest testObject = new DaoRequest();
		try {
			testObject.bookRequestFromDb(Integer.MIN_VALUE);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid Book ID entered", e.getMessage());
		}
	}

	// ///////////////////////////////////////////////////////////

	// Test number: 39
	// Test objective: Test valid input
	// Input(s): Book id = 1
	// Expected Output(s): RequestFromDB object
	// Added by: Shane
	public void test39() throws Exception {
		DaoRequest testObject = new DaoRequest();
		try {
			testObject.journalRequestFromDb(1);
			assertNotNull(testObject);
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 40
	// Test objective: Test valid input
	// Input(s): Book id = max int
	// Expected Output(s): RequestFromDB object
	// Added by: Shane
	public void test40() throws Exception {
		DaoRequest testObject = new DaoRequest();
		try {
			testObject.journalRequestFromDb(Integer.MAX_VALUE);
			assertNotNull(testObject);
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 41
	// Test objective: Test invalid input
	// Input(s): Book id = 0
	// Expected Output(s): -
	// Added by: Shane
	public void test41() throws Exception {
		DaoRequest testObject = new DaoRequest();
		try {
			testObject.journalRequestFromDb(0);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid Book ID entered", e.getMessage());
		}
	}

	// Test number: 42
	// Test objective: Test invalid input
	// Input(s): Book id = -1
	// Expected Output(s): -
	// Added by: Shane
	public void test42() throws Exception {
		DaoRequest testObject = new DaoRequest();
		try {
			testObject.journalRequestFromDb(-1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid Book ID entered", e.getMessage());
		}
	}

	// Test number: 43
	// Test objective: Test invalid input
	// Input(s): Book id = min int
	// Expected Output(s): -
	// Added by: Shane
	public void test43() throws Exception {
		DaoRequest testObject = new DaoRequest();
		try {
			testObject.journalRequestFromDb(Integer.MIN_VALUE);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid Book ID entered", e.getMessage());
		}
	}

	// ///////////////////////////////////////////////////////////
}
