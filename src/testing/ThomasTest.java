package testing;

import dao.DaoJournal;
import junit.framework.TestCase;

public class ThomasTest extends TestCase {
	public void test1104() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(-1, null, null, 1, 1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 5
	// Test objective: Test invalid input
	// Input(s): Journal id = MIN_INT
	// Expected Output(s): Boolean = false

	public void test1105() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(Integer.MIN_VALUE, null, null, 1, 1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 6
	// Test objective: Test Update valid input
	// Input(s): library number = MAX_INT
	// Expected Output(s): Boolean = true

	public void test1116() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true, testObject.updateJournal(1, null, null,
					Integer.MAX_VALUE, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 7
	// Test objective: Test Update valid input
	// Input(s): library number = 1
	// Expected Output(s): Boolean = true

	public void test1117() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true,
					testObject.updateJournal(1, null, null, 1, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 8
	// Test objective: Test Update invalid input
	// Input(s): library number = -1
	// Expected Output(s): Boolean = false

	public void test1118() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, -1, 0, 0, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 9
	// Test objective: Test Update invalid input
	// Input(s): library number = MIN_INT
	// Expected Output(s): Boolean = false

	public void test1119() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, Integer.MIN_VALUE, 1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 10
	// Test objective: Test Update invalid input
	// Input(s): library number = 0
	// Expected Output(s): Boolean = false

	public void test1120() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 0, 1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 11
	// Test objective: Test Update invalid input
	// Input(s): ISBN = -1
	// Expected Output(s): Boolean = false

	public void test1271() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 0, -1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 12
	// Test objective: Test Update invalid input
	// Input(s): ISBN = Integer.MAX_VALUE
	// Expected Output(s): Boolean = false

	public void test1272() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 1, Integer.MAX_VALUE, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 13
	// Test objective: Test Update valid input
	// Input(s): ISBN = 1
	// Expected Output(s): Boolean = true

	public void test1273() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true,
					testObject.updateJournal(1, null, null, 1, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 14
	// Test objective: Test Update invalid input
	// Input(s): ISBN = 0
	// Expected Output(s): Boolean = false

	public void test1274() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 1, 0, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 15
	// Test objective: Test Update invalid input
	// Input(s): ISBN = Integer.MIN_VALUE
	// Expected Output(s): Boolean = false

	public void test1275() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 1, Integer.MIN_VALUE, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 16
	// Test objective: Test Update invalid input
	// Input(s): year = -1
	// Expected Output(s): Boolean = false
	public void test1313() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 1, 1, 1, -1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 17
	// Test objective: Test Update invalid input
	// Input(s): year = 0
	// Expected Output(s): Boolean = false

	public void test1314() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 1, 1, 1, 0);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 18
	// Test objective: Test Update valid input
	// Input(s): year = 1
	// Expected Output(s): Boolean = true

	public void test1315() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true,
					testObject.updateJournal(1, null, null, 0, 0, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 19
	// Test objective: Test Update invalid input
	// Input(s): year = Integer.MIN_VALUE
	// Expected Output(s): Boolean = false

	public void test1316() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 1, 1, 1, Integer.MIN_VALUE);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 20
	// Test objective: Test Update invalid input
	// Input(s): year = Integer.MAX_VALUE
	// Expected Output(s): Boolean = false

	public void test1317() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 1, 1, 1, Integer.MAX_VALUE);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 21
	// Test objective: Test Update invalid input
	// Input(s): RefNo = -1
	// Expected Output(s): Boolean = false
	public void test1213() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 1, 1, -1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 22
	// Test objective: Test Update invalid input
	// Input(s): RefNo = 0
	// Expected Output(s): Boolean = false

	public void test01214() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 1, 1, 0, -1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 23
	// Test objective: Test Update valid input
	// Input(s): RefNo = 1
	// Expected Output(s): Boolean = true

	public void test01215() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 1, 1, 1, 0);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 24
	// Test objective: Test Update invalid input
	// Input(s): RefNo = Integer.MIN_VALUE
	// Expected Output(s): Boolean = false

	public void test00216() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 1, 1, Integer.MIN_VALUE, 0);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 25
	// Test objective: Test Update invalid input
	// Input(s): RefNo = Integer.MAX_VALUE
	// Expected Output(s): Boolean = false

	public void test1204() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 1, 1, Integer.MAX_VALUE, 0);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 26
	// Test objective: Test valid input
	// Input(s): Journal id = 1
	// Expected Output(s): Boolean = true

	public void test1714() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true, testObject.addJournal(1, null, null, 1, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 27
	// Test objective: Test valid input
	// Input(s): Journal id = MAX_INT
	// Expected Output(s): Boolean = true

	public void test1715() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true, testObject.addJournal(Integer.MAX_VALUE, null,
					null, 1, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 28
	// Test objective: Test invalid input
	// Input(s): Journal id = 0
	// Expected Output(s): Boolean = false
	public void test1716() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 1, 1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 29
	// Test objective: Test invalid input
	// Input(s): Journal id = -1
	// Expected Output(s): Boolean = false

	public void test1717() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(-1, null, null, 1, 1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 30
	// Test objective: Test invalid input
	// Input(s): Journal id = MIN_INT
	// Expected Output(s): Boolean = false

	public void test1718() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(Integer.MIN_VALUE, null, null, 1, 1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 31
	// Test objective: Test Update valid input
	// Input(s): library number = MAX_INT
	// Expected Output(s): Boolean = true

	public void test1756() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true, testObject.addJournal(1, null, null,
					Integer.MAX_VALUE, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 32
	// Test objective: Test Update valid input
	// Input(s): library number = 1
	// Expected Output(s): Boolean = true

	public void test1757() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true, testObject.addJournal(1, null, null, 1, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 33
	// Test objective: Test Update invalid input
	// Input(s): library number = -1
	// Expected Output(s): Boolean = false

	public void test1758() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, -1, 0, 0, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 34
	// Test objective: Test Update invalid input
	// Input(s): library number = MIN_INT
	// Expected Output(s): Boolean = false

	public void test1759() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, Integer.MIN_VALUE, 1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 35
	// Test objective: Test Update invalid input
	// Input(s): library number = 0
	// Expected Output(s): Boolean = false

	public void test1760() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 0, 1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 36
	// Test objective: Test Update invalid input
	// Input(s): ISBN = -1
	// Expected Output(s): Boolean = false

	public void test0221() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 0, -1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 37
	// Test objective: Test Update invalid input
	// Input(s): ISBN = Integer.MAX_VALUE
	// Expected Output(s): Boolean = false

	public void test0222() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 1, Integer.MAX_VALUE, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 38
	// Test objective: Test Update valid input
	// Input(s): ISBN = 1
	// Expected Output(s): Boolean = true

	public void test0223() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true, testObject.addJournal(1, null, null, 1, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 39
	// Test objective: Test Update invalid input
	// Input(s): ISBN = 0
	// Expected Output(s): Boolean = false

	public void test0224() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 1, 0, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 40
	// Test objective: Test Update invalid input
	// Input(s): ISBN = Integer.MIN_VALUE
	// Expected Output(s): Boolean = false

	public void test0225() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 1, Integer.MIN_VALUE, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 41
	// Test objective: Test Update invalid input
	// Input(s): year = -1
	// Expected Output(s): Boolean = false
	public void test0323() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 1, 1, 1, -1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 42
	// Test objective: Test Update invalid input
	// Input(s): year = 0
	// Expected Output(s): Boolean = false

	public void test00324() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 1, 1, 1, 0);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 43
	// Test objective: Test Update valid input
	// Input(s): year = 1
	// Expected Output(s): Boolean = true

	public void test00325() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true, testObject.addJournal(1, null, null, 0, 0, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 44
	// Test objective: Test Update invalid input
	// Input(s): year = Integer.MIN_VALUE
	// Expected Output(s): Boolean = false

	public void test0326() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 1, 1, 1, Integer.MIN_VALUE);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 45
	// Test objective: Test Update invalid input
	// Input(s): year = Integer.MAX_VALUE
	// Expected Output(s): Boolean = false

	public void test0327() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 1, 1, 1, Integer.MAX_VALUE);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 46
	// Test objective: Test Update invalid input
	// Input(s): RefNo = -1
	// Expected Output(s): Boolean = false
	public void test0233() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 1, 1, -1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 47
	// Test objective: Test Update invalid input
	// Input(s): RefNo = 0
	// Expected Output(s): Boolean = false

	public void test00234() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 1, 1, 0, -1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 48
	// Test objective: Test Update valid input
	// Input(s): RefNo = 1
	// Expected Output(s): Boolean = true

	public void test00235() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 1, 1, 1, 0);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 49
	// Test objective: Test Update invalid input
	// Input(s): RefNo = Integer.MIN_VALUE
	// Expected Output(s): Boolean = false

	public void test00236() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 1, 1, Integer.MIN_VALUE, 0);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 50
	// Test objective: Test Update invalid input
	// Input(s): RefNo = Integer.MAX_VALUE
	// Expected Output(s): Boolean = false

	public void test0237() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 1, 1, Integer.MAX_VALUE, 0);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}
}
