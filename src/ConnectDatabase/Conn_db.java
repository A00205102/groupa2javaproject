package ConnectDatabase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import testing.ExceptionHandlerClass;

public class Conn_db {
	protected static Connection con;
	String url = null;
	protected static Statement stmt;

	public static Statement connection(String name)
			throws ClassNotFoundException {
		// url = "jdbc:mysql://localhost:3306/Lois"
		// +
		// "user=root & password=883883 & useUnicode=true&characterEnunicode=UTF8&useSSL=false";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String url = name;

			con = DriverManager.getConnection(url, "root", "");
			System.out.println("connect to database successfully");
			stmt = con.createStatement();
		} catch (Exception e) {
			System.out.println("Error: Failed to connect to database\n"
					+ e.getMessage());
			System.out.println("Errorxxxxx");
			// e.printStackTrace();
		}
		return stmt;
	}

	public static boolean getConn(String name) throws ExceptionHandlerClass {
		boolean res = false;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String url = name;

			con = DriverManager.getConnection(url, "root", "");
			System.out.println("connect to database successfully");
			stmt = con.createStatement();
			res = true;
		} catch (Exception e) {
			System.out.println("Error: Failed to connect to database\n"
					+ e.getMessage());
			System.out.println("Errorxxxxx");
			res = false;
			// e.printStackTrace();
		}
		return res;
	}

	public void updateDB(String updateStmt) {

		try {
			connection("jdbc:mysql://127.0.0.1:3307/Agile");
			stmt.executeUpdate(updateStmt);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void viewRequest(QueryTableModel tableModel, String option) {
		try {
			connection("jdbc:mysql://127.0.0.1:3307/Agile");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		tableModel.refreshFromDB(stmt, option);
	}

}