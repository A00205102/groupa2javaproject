package dao;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import ConnectDatabase.Conn_db;
import classFolder.Journal;
import testing.ExceptionHandlerClass;

public class DaoJournal extends Conn_db {

	JTextField id, name, author, Libraryno, ISBN, RefNo, RealY;
	JButton Add, Reset;
	static Statement stmt;
	ResultSet rs;

	public Journal journalDetails(String name) throws ClassNotFoundException,
			SQLException {
		Journal j = new Journal();

		connection("jdbc:mysql://127.0.0.1:3307/Agile");
		String cmd = "select * from Journals where Journal_Name = '" + name
				+ "';";
		rs = stmt.executeQuery(cmd);

		while (rs.next()) {
			int Jid = Integer.parseInt(rs.getString(1));
			String Jname = rs.getString(2);
			String author = rs.getString(3);
			int libNo = Integer.parseInt(rs.getString(4));
			int isbnNo = Integer.parseInt(rs.getString(5));
			int refNo = Integer.parseInt(rs.getString(6));
			int year = Integer.parseInt(rs.getString(7));
			j.setID(Jid);
			j.setName(Jname);
			j.setAuthor(author);
			j.setLibraryNo(libNo);
			j.setISBN_No(isbnNo);
			j.setRef_No(refNo);
			j.setrelease_Year(year);
		}
		return j;
	}

	// add book method added by Thomas
	// Partial Product Skeleton Code
	public static boolean addJournal(int id, String name, String author,
			int libraryNum, int isbn, int refNum, int year)
			throws ExceptionHandlerClass, SQLException {

		stmt = con.createStatement();
		if (id == 0) {
			throw new ExceptionHandlerClass("Invalid value entered");
		} else if (id < 0) {
			throw new ExceptionHandlerClass("Invalid value entered");
		} else if (id > 0) {
			try {
				String sql = "INSERT INTO Journals VALUES(" + null + ",'"
						+ name + "','" + author + "'," + libraryNum + ","
						+ isbn + "," + refNum + "," + year + ");";
				stmt.executeUpdate(sql);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return true;
		} else {
			return false;
		}
	}

	// update book method added by Thomas
	// Partial Product Skeleton Code
	public static boolean updateJournal(int id, String name, String author,
			int libraryNum, int isbn, int refNum, int year)
			throws ExceptionHandlerClass, Exception {

		stmt = con.createStatement();
		if (id == 0) {
			throw new ExceptionHandlerClass("Invalid value entered");
		} else if (id < 0) {
			throw new ExceptionHandlerClass("Invalid value entered");
		} else if (id > 0) {
			try {
				String query = "UPDATE Journals SET Journal_Name='" + name
						+ "', Author='" + author + "', Library_Number='"
						+ libraryNum + "', ISBN_Number='" + isbn
						+ "', Ref_Number='" + refNum + "', Release_Year='"
						+ year + "' WHERE Journal_id=" + id + ";";
				stmt.executeUpdate(query);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return true;
		} else {
			return false;
		}
	}

	public boolean searchJournal(String jname) {
		Conn_db obj = new Conn_db();
		boolean res = false;
		try {
			stmt = (Statement) obj
					.connection("jdbc:mysql://127.0.0.1:3307/Agile");
			String cmd = "select * from Journals where Journal_Name = '"
					+ jname + "';";
			rs = stmt.executeQuery(cmd);

			while (rs.next()) {
				String Jname = rs.getString(2);
				if (Jname != null) {
					res = true;
				} else {
					res = false;
				}
			}

		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}

		return res;
		// return true;/
	}

	public boolean placeJournal(int id, String title, String author, int isbn)
			throws ExceptionHandlerClass, Exception {
		Conn_db connection = new Conn_db();
		Statement stmt2;
		if (id == 0) {
			throw new ExceptionHandlerClass(
					"Invalid Journal ID entered, please enter a value greater than zero");
		} else if (id < 0) {
			throw new ExceptionHandlerClass("Invalid Journal ID entered");
		} else if (id > 0) {
			try {
				stmt2 = connection
						.connection("jdbc:mysql://127.0.0.1:3307/Agile");

				String updateTemp = "INSERT INTO req_Journal(id,title,author,ISBN) VALUES("
						+ id
						+ ",'"
						+ title
						+ "','"
						+ author
						+ "',"
						+ isbn
						+ ");";
				System.out.println(updateTemp);
				stmt2.executeUpdate(updateTemp);
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
			return true;
		} else {
			return false;
		}
	}

}
