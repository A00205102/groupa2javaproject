package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;

import classFolder.Student;
import dao.DaoStudent;

import javax.swing.JScrollPane;

public class StudentMenu extends DaoStudent {

	private JFrame frame;
	private static String username;
	private static String UserDetails;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StudentMenu window = new StudentMenu(username);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public StudentMenu(String Username) {
		initialize();
		this.username = Username;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		JLabel lblWelcomeToStudent = new JLabel("Welcome To Student Menu ");
		lblWelcomeToStudent.setBounds(103, 27, 193, 15);
		panel.add(lblWelcomeToStudent);

		JButton btnSearch = new JButton("Search ");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Search s = new Search();
				s.main(null);
			}
		});
		btnSearch.setBounds(255, 68, 152, 25);
		panel.add(btnSearch);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(45, 68, 152, 129);
		panel.add(scrollPane);

		JTextArea textArea = new JTextArea();
		scrollPane.setViewportView(textArea);

		JButton btnViewDetails = new JButton("View Details");
		btnViewDetails.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Student s = new Student();
				try {
					s = viewDetails(username);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				UserDetails = "Your Details are \n" + "------------------- \n"
						+ "Username:  " + s.getUsername() + "\n" + "Funds:   "
						+ s.getFunds() + "\n" + "Fines:   " + s.getFines()
						+ "\n";

				textArea.append(UserDetails);
			}
		});
		btnViewDetails.setBounds(45, 209, 152, 25);
		panel.add(btnViewDetails);

		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnExit.setBounds(255, 209, 152, 25);
		panel.add(btnExit);

		JButton btnNewButton = new JButton("Place Request");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PlaceReqGUI req = new PlaceReqGUI(username);
				req.main(null);
				frame.setVisible(false);
			}
		});
		btnNewButton.setBounds(255, 117, 152, 25);
		panel.add(btnNewButton);

		JButton btnLogout = new JButton("Logout");
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LoginStu ls = new LoginStu();
				ls.main(null);
				frame.setVisible(false);
			}
		});
		btnLogout.setBounds(255, 161, 152, 25);
		panel.add(btnLogout);

	}
}
