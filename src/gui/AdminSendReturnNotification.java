package gui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import classFolder.RequestFromDB;
import dao.DaoRequest;
import testing.ExceptionHandlerClass;

public class AdminSendReturnNotification {

	private JFrame frame;
	private JTextField bookTextField;
	private JTextField expirydatetextfield;
	private JTextField emailtextfield;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminSendReturnNotification window = new AdminSendReturnNotification();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AdminSendReturnNotification() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 376, 315);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		bookTextField = new JTextField();
		bookTextField.setBounds(110, 11, 200, 23);
		frame.getContentPane().add(bookTextField);
		bookTextField.setColumns(10);

		JLabel lblEnterIdOf = new JLabel(
				"Enter Book Title:");
		lblEnterIdOf.setBounds(10, 11, 359, 23);
		frame.getContentPane().add(lblEnterIdOf);




		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			@SuppressWarnings("static-access")
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
				AdminMenu menu = new AdminMenu();
				menu.main(null);
			}
		});
		btnBack.setBounds(142, 242, 89, 23);
		frame.getContentPane().add(btnBack);
		
		expirydatetextfield = new JTextField();
		expirydatetextfield.setBounds(110, 45, 200, 20);
		frame.getContentPane().add(expirydatetextfield);
		expirydatetextfield.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Expiry Date:");
		lblNewLabel.setBounds(10, 48, 65, 14);
		frame.getContentPane().add(lblNewLabel);
		
		emailtextfield = new JTextField();
		emailtextfield.setBounds(110, 76, 200, 20);
		frame.getContentPane().add(emailtextfield);
		emailtextfield.setColumns(10);
		
		JButton btnNewButton_1 = new JButton("Send Notification");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String title= bookTextField.getText();
				String expiry = expirydatetextfield.getText();
				String email = emailtextfield.getText();
				
				DaoRequest.sendEmailNotificationForExpiry(title, expiry, email);
			}
		});
		btnNewButton_1.setBounds(120, 107, 163, 23);
		frame.getContentPane().add(btnNewButton_1);
		
		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setBounds(10, 79, 46, 14);
		frame.getContentPane().add(lblEmail);
	}
}
