package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.Border;

import ConnectDatabase.QueryTableModel;
import ConnectDatabase.Conn_db;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ViewReqGUI {

	private JFrame frame;
	private QueryTableModel tableModel = new QueryTableModel();
	private JTable table = new JTable(tableModel);
	private JScrollPane pane;
	private JButton btnViewJournal;
	private JButton btnBack;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewReqGUI window = new ViewReqGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ViewReqGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 749, 529);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		table.setPreferredScrollableViewportSize(new Dimension(900, 300));

		pane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		pane.setBackground(Color.lightGray);
		Border lineBorder = BorderFactory.createEtchedBorder(15, Color.red,
				Color.black);
		pane.setBorder(BorderFactory.createTitledBorder(lineBorder,
				"Database Content"));
		pane.setSize(700, 300);
		pane.setVisible(true);
		frame.getContentPane().add(pane);

		JButton btnNewButton = new JButton("View Book");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Conn_db connection = new Conn_db();
				String option = "book";
				connection.viewRequest(tableModel, option);

			}
		});
		btnNewButton.setBounds(78, 370, 178, 47);
		frame.getContentPane().add(btnNewButton);

		btnViewJournal = new JButton("View Journal");
		btnViewJournal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Conn_db connection = new Conn_db();
				String option = "journal";
				connection.viewRequest(tableModel, option);
			}
		});
		btnViewJournal.setBounds(438, 370, 178, 47);
		frame.getContentPane().add(btnViewJournal);

		btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminMenu am = new AdminMenu();
				am.main(null);
				frame.setVisible(false);
			}
		});
		btnBack.setBounds(258, 440, 178, 47);
		frame.getContentPane().add(btnBack);
	}
}
