package gui;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Window.Type;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import dao.DaoBook;
import testing.ExceptionHandlerClass;

public class AddBookGUI {

	private JFrame AddBookWindow;
	private JTextField idTextBox;
	private JTextField bookNameTextBox;
	private JTextField authorTextBox;
	private JTextField libraryNumberTextBox;
	private JTextField isbnTextBox;
	private JTextField refNumberTextBox;
	private JTextField releaseYearTextBox;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddBookGUI window = new AddBookGUI();
					window.AddBookWindow.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AddBookGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		AddBookWindow = new JFrame();
		AddBookWindow.setTitle("Add Book");
		AddBookWindow
				.setFont(new Font("Arial Rounded MT Bold", Font.PLAIN, 12));
		AddBookWindow.setBounds(100, 100, 300, 325);
		AddBookWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		AddBookWindow.getContentPane().setLayout(null);

		// JLabel idLabel = new JLabel("Enter Id of book: ");
		// idLabel.setBounds(23, 36, 132, 14);
		// AddBookWindow.getContentPane().add(idLabel);

		// idTextBox = new JTextField();
		// idTextBox.setBounds(152, 34, 122, 20);
		// AddBookWindow.getContentPane().add(idTextBox);
		// idTextBox.setColumns(10);

		JLabel idLabel1 = new JLabel("Book Name:");
		idLabel1.setBounds(23, 65, 132, 14);
		AddBookWindow.getContentPane().add(idLabel1);

		bookNameTextBox = new JTextField();
		bookNameTextBox.setBounds(152, 63, 122, 20);
		AddBookWindow.getContentPane().add(bookNameTextBox);
		bookNameTextBox.setColumns(10);

		JLabel idLabel2 = new JLabel("Book Author:");
		idLabel2.setBounds(23, 97, 132, 14);
		AddBookWindow.getContentPane().add(idLabel2);

		authorTextBox = new JTextField();
		authorTextBox.setBounds(152, 94, 122, 20);
		AddBookWindow.getContentPane().add(authorTextBox);
		authorTextBox.setColumns(10);

		JLabel idLabel3 = new JLabel("Library Number:");
		idLabel3.setBounds(23, 127, 132, 14);
		AddBookWindow.getContentPane().add(idLabel3);

		libraryNumberTextBox = new JTextField();
		libraryNumberTextBox.setBounds(152, 125, 122, 20);
		AddBookWindow.getContentPane().add(libraryNumberTextBox);
		libraryNumberTextBox.setColumns(10);

		JLabel idLabel4 = new JLabel("ISBN Number:");
		idLabel4.setBounds(23, 157, 132, 14);
		AddBookWindow.getContentPane().add(idLabel4);

		isbnTextBox = new JTextField();
		isbnTextBox.setBounds(152, 156, 122, 20);
		AddBookWindow.getContentPane().add(isbnTextBox);
		isbnTextBox.setColumns(10);

		JLabel idLabel5 = new JLabel("Reference Number:");
		idLabel5.setBounds(23, 187, 132, 14);
		AddBookWindow.getContentPane().add(idLabel5);

		refNumberTextBox = new JTextField();
		refNumberTextBox.setBounds(152, 187, 122, 20);
		AddBookWindow.getContentPane().add(refNumberTextBox);
		refNumberTextBox.setColumns(10);

		JLabel idLabel6 = new JLabel("Release Year:");
		idLabel6.setBounds(23, 217, 132, 14);
		AddBookWindow.getContentPane().add(idLabel6);

		releaseYearTextBox = new JTextField();
		releaseYearTextBox.setBounds(152, 218, 122, 20);
		AddBookWindow.getContentPane().add(releaseYearTextBox);
		releaseYearTextBox.setColumns(10);

		JLabel infoLabel = new JLabel("Please add book details:");
		infoLabel.setBounds(82, 11, 192, 14);
		AddBookWindow.getContentPane().add(infoLabel);

		JButton backButton = new JButton("Back");
		backButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AddBookWindow.setVisible(false);
				AdminMenu menu = new AdminMenu();
				menu.main(null);
			}
		});
		backButton.setBounds(10, 247, 122, 23);
		AddBookWindow.getContentPane().add(backButton);

		JButton addButton = new JButton("Add");
		addButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = bookNameTextBox.getText();
				String author = authorTextBox.getText();
				int libNum = Integer.parseInt(libraryNumberTextBox.getText());
				int isbn = Integer.parseInt(isbnTextBox.getText());
				int refNum = Integer.parseInt(refNumberTextBox.getText());
				int relYear = Integer.parseInt(releaseYearTextBox.getText());

				DaoBook book = new DaoBook();
				try {
					book.addBook(name, author, libNum, isbn, refNum, relYear);
				} catch (ExceptionHandlerClass e1) {
					e1.printStackTrace();
				}

			}
		});
		addButton.setBounds(152, 249, 122, 23);
		AddBookWindow.getContentPane().add(addButton);
	}

}
