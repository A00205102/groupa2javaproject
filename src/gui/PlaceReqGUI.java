package gui;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JRadioButton;

import ConnectDatabase.Conn_db;

import java.sql.*;

import gui.PlaceReqGUI;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class PlaceReqGUI extends Conn_db {

	private Connection con = null;
	private static Statement stmt = null;
	private ResultSet rs = null;

	private JFrame frame;
	private JTextField textFid;
	private JTextField textFtitle;
	private JTextField textFisbn;
	private JTextField textFauthor;
	private JTextField textFemail;
	private static String username;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PlaceReqGUI window = new PlaceReqGUI(username);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public PlaceReqGUI(String user) {

		username = user;

		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.getContentPane().setLayout(null);
		frame.setBackground(Color.CYAN);

		JButton btnPlaceReq_book = new JButton("Place Book");
		btnPlaceReq_book.setBounds(12, 256, 165, 23);
		frame.getContentPane().add(btnPlaceReq_book);

		textFid = new JTextField();
		textFid.setBounds(125, 60, 137, 23);
		frame.getContentPane().add(textFid);
		textFid.setColumns(10);

		textFtitle = new JTextField();
		textFtitle.setBounds(125, 96, 137, 23);
		frame.getContentPane().add(textFtitle);
		textFtitle.setColumns(10);

		textFisbn = new JTextField();
		textFisbn.setColumns(10);
		textFisbn.setBounds(125, 174, 137, 23);
		frame.getContentPane().add(textFisbn);

		textFauthor = new JTextField();
		textFauthor.setBounds(125, 138, 137, 23);
		frame.getContentPane().add(textFauthor);
		textFauthor.setColumns(10);

		JLabel lblTitle = new JLabel("ID");
		lblTitle.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblTitle.setBounds(31, 65, 86, 14);
		frame.getContentPane().add(lblTitle);

		JLabel lblIsbn = new JLabel("Title");
		lblIsbn.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblIsbn.setBounds(31, 103, 86, 14);
		frame.getContentPane().add(lblIsbn);

		JLabel lblId = new JLabel("Author");
		lblId.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblId.setBounds(31, 143, 86, 14);
		frame.getContentPane().add(lblId);

		JLabel lblAuthor = new JLabel("ISBN");
		lblAuthor.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblAuthor.setBounds(31, 179, 86, 14);
		frame.getContentPane().add(lblAuthor);

		JButton btnPlaceReq_journal = new JButton("Place Journal");
		btnPlaceReq_journal.setBounds(12, 300, 165, 23);
		frame.getContentPane().add(btnPlaceReq_journal);

		JLabel lblPlaceRequest = new JLabel("Place Request ");
		lblPlaceRequest.setFont(new Font("Times New Roman", Font.BOLD, 16));
		lblPlaceRequest.setBounds(98, 11, 204, 23);
		frame.getContentPane().add(lblPlaceRequest);

		JButton btnClear = new JButton("Clear");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFid.setText("");
				textFtitle.setText("");
				textFisbn.setText("");
				textFauthor.setText("");
				textFemail.setText("");

			}
		});
		btnClear.setBounds(197, 255, 86, 25);
		frame.getContentPane().add(btnClear);

		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnExit.setBounds(125, 333, 86, 25);
		frame.getContentPane().add(btnExit);

		JLabel lblEmail = new JLabel("Email");
		lblEmail.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblEmail.setBounds(31, 214, 86, 14);
		frame.getContentPane().add(lblEmail);

		textFemail = new JTextField();
		textFemail.setColumns(10);
		textFemail.setBounds(125, 209, 137, 23);
		frame.getContentPane().add(textFemail);

		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				StudentMenu sm = new StudentMenu(username);
				sm.main(null);
				frame.setVisible(false);

			}
		});
		btnBack.setBounds(207, 299, 86, 25);
		frame.getContentPane().add(btnBack);

		btnPlaceReq_book.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Conn_db conn = new Conn_db();
				try {
					stmt = (Statement) conn
							.connection("jdbc:mysql://127.0.0.1:3307/Agile");
				} catch (ClassNotFoundException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}

				String updateTemp = "INSERT INTO req_book VALUES("
						+ textFid.getText() + ",'" + textFtitle.getText()
						+ "','" + textFauthor.getText() + "',"
						+ textFisbn.getText() + ",'" + textFemail.getText()
						+ "');";
				System.out.println(updateTemp);
				try {
					stmt.executeUpdate(updateTemp);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		btnPlaceReq_journal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Conn_db conn = new Conn_db();
				try {
					stmt = (Statement) conn
							.connection("jdbc:mysql://127.0.0.1:3307/Agile");
				} catch (ClassNotFoundException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}

				String updateTemp = "INSERT INTO req_journal VALUES("
						+ textFid.getText() + ",'" + textFtitle.getText()
						+ "','" + textFauthor.getText() + "',"
						+ textFisbn.getText() + ",'" + textFemail.getText()
						+ "');";
				System.out.println(updateTemp);
				try {
					stmt.executeUpdate(updateTemp);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		frame.setSize(313, 400);
		frame.setVisible(true);
	}
}
