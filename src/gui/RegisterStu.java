package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import dao.DaoStudent;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class RegisterStu extends DaoStudent {

	private JFrame frame;
	private JTextField username;
	private JTextField password;
	private JTextField funds;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegisterStu window = new RegisterStu();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public RegisterStu() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(34, 50, 110, 15);
		panel.add(lblUsername);

		username = new JTextField();
		username.setBounds(140, 48, 200, 28);
		panel.add(username);
		username.setColumns(10);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(34, 98, 110, 15);
		panel.add(lblPassword);

		password = new JTextField();
		password.setColumns(10);
		password.setBounds(140, 96, 200, 28);
		panel.add(password);

		JLabel lblFunds = new JLabel("Funds");
		lblFunds.setBounds(34, 143, 110, 15);
		panel.add(lblFunds);

		funds = new JTextField();
		funds.setColumns(10);
		funds.setBounds(140, 141, 200, 28);
		panel.add(funds);

		JButton btnRegister = new JButton("Register");
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String user = username.getText();
				String pass = password.getText();
				String f = funds.getText();
				double fund = Double.parseDouble(f);
				boolean res = register(user, pass, fund);
				if (res == true) {
					LoginStu sl = new LoginStu();
					sl.main(null);
				} else {
					JOptionPane.showMessageDialog(null, "Register failed");
					username.setText("");
					password.setText("");
					funds.setText("");
				}
			}
		});
		btnRegister.setBounds(239, 199, 117, 25);
		panel.add(btnRegister);

		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				LoginStu log = new LoginStu();
				log.main(null);
			}
		});
		btnBack.setBounds(49, 199, 117, 25);
		panel.add(btnBack);
	}

}
