package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;

import java.awt.FlowLayout;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;

import dao.DaoFine;
import dao.DaoFund;

public class AddFundGUI {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddFundGUI window = new AddFundGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AddFundGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 498);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(154, 41, 145, 24);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblEnterStudentId = new JLabel("Enter Student Id:");
		lblEnterStudentId.setBounds(14, 38, 136, 31);
		frame.getContentPane().add(lblEnterStudentId);
		
		JLabel lblStudentName = new JLabel("Student Name:");
		lblStudentName.setBounds(27, 114, 123, 18);
		frame.getContentPane().add(lblStudentName);
		
		textField_1 = new JTextField();
		textField_1.setBounds(154, 111, 145, 24);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblFund = new JLabel("Fund:");
		lblFund.setBounds(58, 170, 51, 31);
		frame.getContentPane().add(lblFund);
		
		textField_2 = new JTextField();
		textField_2.setBounds(154, 173, 145, 24);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
				
			}
		});
		btnBack.setBounds(14, 318, 113, 27);
		frame.getContentPane().add(btnBack);
		
		final JButton btnAdd = new JButton("Add Fund");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int id = Integer.parseInt(textField.getText());
				String name = textField_1.getText();
				double fine = Double.parseDouble(textField_2.getText());

				try {
					DaoFund book = new DaoFund();
					book.AddFund(id, name, fine);
					JOptionPane.showMessageDialog(btnAdd,
							"Add Funds Successful");
					clearFields();
				}
			
				catch (Exception e) {
					e.printStackTrace();
				}
			}

			private void clearFields() {
				textField.setText("");
				textField_1.setText("");
				textField_2.setText("");

			}
		});
		btnAdd.setBounds(245, 318, 113, 27);
		frame.getContentPane().add(btnAdd);
	}
}
