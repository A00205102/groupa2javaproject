package testing;

import static org.junit.Assert.*;
import ConnectDatabase.DaoviewReq;
import dao.DaoBook;
import dao.DaoJournal;
//import dao.DaoLogin;
import dao.Register_db;
import dao.Login_db;
import ConnectDatabase.Conn_db;
import ConnectDatabase.QueryTableModel;
import junit.framework.TestCase;
import testing.ExceptionHandlerClass;

public class Testing extends TestCase {

	// ////////////student register
	// Test number: 1 student register
	// Test objective: Test valid input
	// Input(s): username="Lois",password="123"
	// Expected Output(s): Boolean = true
	// Added by: Lois
	public void test101() {
		Register_db testObject = new Register_db();
		try {
			assertTrue(testObject.writeInSql("Lois", "123"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 2
	// Test objective: Test invalid input
	// Input(s): username="Lois",password=""
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test102() {
		Register_db testObject = new Register_db();
		try {
			assertFalse(testObject.writeInSql("Lois", ""));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 3
	// Test objective: Test invalid input
	// Input(s): username="",password="123"
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test103() {
		Register_db testObject = new Register_db();
		try {
			assertFalse(testObject.writeInSql("", "123"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 4
	// Test objective: Test invalid input
	// Input(s): username="",password=""
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test104() {
		Register_db testObject = new Register_db();
		try {
			assertFalse(testObject.writeInSql("", ""));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// ////admin register
	// Test number: 5 admin register
	// Test objective: Test valid input
	// Input(s): username="admin",password="pass"
	// Expected Output(s): Boolean = true
	// Added by: Lois
	public void test105() {
		Register_db testObject = new Register_db();
		try {
			assertTrue(testObject.writeInSql("admin", "pass"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 6
	// Test objective: Test invalid input
	// Input(s): username="admin",password=""
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test106() {
		Register_db testObject = new Register_db();
		try {
			assertFalse(testObject.writeInSql("admin", ""));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 7
	// Test objective: Test invalid input
	// Input(s): username="",password="pass"
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test107() {
		Register_db testObject = new Register_db();
		try {
			assertFalse(testObject.writeInSql("", "pass"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 8
	// Test objective: Test invalid input
	// Input(s): username="",password="123"
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test108() {
		Register_db testObject = new Register_db();
		try {
			assertFalse(testObject.writeInSql("", ""));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// /student log in
	// Test number: 9 student log in
	// Test objective: Test valid input
	// Input(s): username="Lois",password="123"
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test109() {
		Login_db testObject = new Login_db();
		try {
			assertTrue(testObject.compareWithSql("Lois", "123"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 10
	// Test objective: Test invalid input
	// Input(s): username="Lois",password="12345"
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test110() {
		Login_db testObject = new Login_db();
		try {
			assertFalse(testObject.compareWithSql("Lois", "12345"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 11
	// Test objective: Test invalid input
	// Input(s): username="lois",password=""
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test111() {
		Login_db testObject = new Login_db();
		try {
			assertFalse(testObject.compareWithSql("Lois", ""));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 12
	// Test objective: Test invalid input
	// Input(s): username="Louise",password="123"
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test112() {
		Register_db testObject = new Register_db();
		try {
			assertFalse(testObject.writeInSql("louise", "123"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 13
	// Test objective: Test invalid input
	// Input(s): username="louise",password="12345"
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test113() {
		Login_db testObject = new Login_db();
		try {
			assertFalse(testObject.compareWithSql("louise", "12345"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 14
	// Test objective: Test invalid input
	// Input(s): username="",password="123"
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test114() {
		Login_db testObject = new Login_db();
		try {
			assertFalse(testObject.compareWithSql("louise", ""));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 15
	// Test objective: Test invalid input
	// Input(s): username="",password="123"
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test115() {
		Login_db testObject = new Login_db();
		try {
			assertFalse(testObject.compareWithSql("", "123"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 16
	// Test objective: Test valid input
	// Input(s): username="",password="12345"
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test1011() {
		Login_db testObject = new Login_db();
		try {
			assertTrue(testObject.compareWithSql("", "12345"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 17
	// Test objective: Test invalid input
	// Input(s): username="",password=""
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test1017() {
		Login_db testObject = new Login_db();
		try {
			assertFalse(testObject.compareWithSql("", ""));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// //////admin log in
	// Test number: 18 admin log in
	// Test objective: Test invalid input
	// Input(s): username="admin",password="pass"
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test1018() {
		Login_db testObject = new Login_db();
		try {
			assertTrue(testObject.compareWithSql("admin", "pass"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 19
	// Test objective: Test invalid input
	// Input(s): username="",password="pass"
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test1019() {
		Login_db testObject = new Login_db();
		try {
			assertFalse(testObject.compareWithSql("admin", "pass123"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 20
	// Test objective: Test invalid input
	// Input(s): username="admin",password=""
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test1020() {
		Login_db testObject = new Login_db();
		try {
			assertFalse(testObject.compareWithSql("admin", ""));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 21
	// Test objective: Test invalid input
	// Input(s): username="admin",password=""
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test1021() {
		Login_db testObject = new Login_db();
		try {
			assertFalse(testObject.compareWithSql("admin", ""));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 22 admin log in
	// Test objective: Test invalid input
	// Input(s): username="admin123",password="pass"
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test1022() {
		Login_db testObject = new Login_db();
		try {
			assertFalse(testObject.compareWithSql("admin123", "pass"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 23 admin log in
	// Test objective: Test invalid input
	// Input(s): username="admin123",password="pass123"
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test1023() {
		Login_db testObject = new Login_db();
		try {
			assertFalse(testObject.compareWithSql("admin123", "pass123"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 24 admin log in
	// Test objective: Test invalid input
	// Input(s): username="admin123",password="pass"
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test1024() {
		Login_db testObject = new Login_db();
		try {
			assertFalse(testObject.compareWithSql("admin123", ""));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 25 admin log in
	// Test objective: Test invalid input
	// Input(s): username="",password="pass"
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test1025() {
		Login_db testObject = new Login_db();
		try {
			assertFalse(testObject.compareWithSql("", "pass"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 26 admin log in
	// Test objective: Test invalid input
	// Input(s): username="",password="pass"
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test1026() {
		Login_db testObject = new Login_db();
		try {
			assertFalse(testObject.compareWithSql("", "pass123"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Test number: 27 admin log in
	// Test objective: Test invalid input
	// Input(s): username="",password="pass"
	// Expected Output(s): Boolean = false
	// Added by: Lois
	public void test1027() {
		Login_db testObject = new Login_db();
		try {
			assertFalse(testObject.compareWithSql("", ""));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// /////////////////////////////////////////////////////////////register
	// login test done

	// Test number: 1
	// Test objective: Test valid input
	// Input(s): Book id = 1
	// Expected Output(s): Boolean = true
	public void test001() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			assertEquals(true, testObject.placeBook(1, null, null, 0));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 2
	// Test objective: Test valid input
	// Input(s): Book id = max_value
	// Expected Output(s): Boolean = true
	public void test002() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			assertEquals(true,
					testObject.placeBook(Integer.MAX_VALUE, null, null, 0));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 3
	// Test objective: Test valid input
	// Input(s): Book id = 100
	// Expected Output(s): Boolean = true
	public void test003() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			assertEquals(true, testObject.placeBook(100, null, null, 0));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 4
	// Test objective: Test invalid input
	// Input(s): Book id = 0
	// Expected Output(s): Boolean = false
	public void test004() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			testObject.placeBook(0, null, null, 0);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals(
					"Invalid Book ID entered, please enter a value greater than zero",
					e.getMessage());
		}
	}

	// Test number: 5
	// Test objective: Test invalid input
	// Input(s): Book id = -1
	// Expected Output(s): Boolean = false
	public void test005() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			testObject.placeBook(-1, null, null, 0);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid Book ID entered", e.getMessage());
		}
	}

	// Test number: 6
	// Test objective: Test invalid input
	// Input(s): Book id = -100
	// Expected Output(s): Boolean = false
	public void test006() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			testObject.placeBook(-100, null, null, 0);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid Book ID entered", e.getMessage());
		}
	}

	// Test number: 7
	// Test objective: Test invalid input
	// Input(s): Book id = MIN value
	// Expected Output(s): Boolean = false
	public void test007() throws Exception {
		DaoBook testObject = new DaoBook();
		try {
			testObject.placeBook(Integer.MIN_VALUE, null, null, 0);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid Book ID entered", e.getMessage());
		}
	}

	// Test number: 8
	// Test objective: Test valid input
	// Input(s): Journal id = 1
	// Expected Output(s): Boolean = true
	public void test008() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true, testObject.placeJournal(1, null, null, 0));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 9
	// Test objective: Test valid input
	// Input(s): Journal id = 100
	// Expected Output(s): Boolean = true
	public void test009() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true, testObject.placeJournal(100, null, null, 0));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 10
	// Test objective: Test valid input
	// Input(s): Journal id = MAX value
	// Expected Output(s): Boolean = true
	public void test00010() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true,
					testObject.placeJournal(Integer.MAX_VALUE, null, null, 0));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 11
	// Test objective: Test invalid input
	// Input(s): Journal id = 0
	// Expected Output(s): Boolean = false
	public void test00011() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.placeJournal(0, null, null, 0);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals(
					"Invalid Journal ID entered, please enter a value greater than zero",
					e.getMessage());
		}
	}

	// Test number: 12
	// Test objective: Test invalid input
	// Input(s):Journal id = -1
	// Expected Output(s): Boolean = false
	public void test00012() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.placeJournal(-1, null, null, 0);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid Journal ID entered", e.getMessage());
		}
	}

	// Test number: 13
	// Test objective: Test invalid input
	// Input(s):Journal id = -100
	// Expected Output(s): Boolean = false
	public void test00013() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.placeJournal(-100, null, null, 0);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid Journal ID entered", e.getMessage());
		}
	}

	// Test number: 14
	// Test objective: Test invalid input
	// Input(s): Journal id = MIN value
	// Expected Output(s): Boolean = false
	public void test00014() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.placeJournal(Integer.MIN_VALUE, null, null, 0);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid Journal ID entered", e.getMessage());
		}
	}

	// Test number: 1
	// Test objective: Test valid input
	// Input(s): option = book
	// Expected Output(s):1
	public void test01() {
		Conn_db con = new Conn_db();
		ConnectDatabase.DaoviewReq h = new ConnectDatabase.DaoviewReq(con);

		QueryTableModel tableModel = new QueryTableModel();
		String option = "book";

		try {
			int result = h.viewRequest(tableModel, option);
			assertEquals(1, result);
		} catch (Exception e) {
			fail("should not come here");
		}
	}

	// Test number: 2
	// Test objective: Test valid input
	// Input(s): option = book
	// Expected Output(s):1

	public void test02() {
		Conn_db con = new Conn_db();
		ConnectDatabase.DaoviewReq h = new ConnectDatabase.DaoviewReq(con);

		QueryTableModel tableModel = new QueryTableModel();
		String option = "journal";

		try {
			int result = h.viewRequest(tableModel, option);
			assertEquals(1, result);
		} catch (Exception e) {
			fail("should not come here");
		}
	}

	// Test number: 3
	// Test objective: Test invalid input
	// Input(s): option = book,tableModel=null;
	// Expected Output(s):-1

	public void test03() {
		Conn_db con = new Conn_db();
		ConnectDatabase.DaoviewReq h = new ConnectDatabase.DaoviewReq(con);
		String option = "book";

		try {
			h.viewRequest(null, option);
			fail("should not come here");
		} catch (Exception e) {
			assertEquals("View Request Exception", e.getMessage());
		}

	}

	// Test number: 4
	// Test objective: Test invalid input
	// Input(s): option = title,;
	// Expected Output(s):-1
	public void test04() {
		Conn_db con = new Conn_db();
		ConnectDatabase.DaoviewReq h = new ConnectDatabase.DaoviewReq(con);
		String option = "title";

		try {
			h.viewRequest(null, option);
			fail("should not come here");
		} catch (Exception e) {
			assertEquals("View Request Exception", e.getMessage());
		}

	}

	// Test number: 5
	// Test objective: Test invalid input
	// Input(s): option = name;
	// Expected Output(s):-1
	public void test05() {
		Conn_db con = new Conn_db();
		ConnectDatabase.DaoviewReq h = new ConnectDatabase.DaoviewReq(con);
		String option = "name";

		try {
			h.viewRequest(null, option);
			fail("should not come here");
		} catch (Exception e) {
			assertEquals("View Request Exception", e.getMessage());
		}

	}

	// Test number: 6
	// Test objective: Test invalid input
	// Input(s): option = Journal,tableModel=null;
	// Expected Output(s):-1

	public void test06() {
		Conn_db con = new Conn_db();
		ConnectDatabase.DaoviewReq h = new ConnectDatabase.DaoviewReq(con);
		String option = "journal";

		try {
			h.viewRequest(null, option);
			fail("should not come here");
		} catch (Exception e) {
			assertEquals("View Request Exception", e.getMessage());
		}

	}

	// Test number: 1

	// Test objective: Test valid input
	// Input(s): Journal id = 1
	// Expected Output(s): Boolean = true

	public void test1101() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true,
					testObject.updateJournal(1, null, null, 1, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 2
	// Test objective: Test valid input
	// Input(s): Journal id = MAX_INT
	// Expected Output(s): Boolean = true

	public void test1102() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true, testObject.updateJournal(Integer.MAX_VALUE,
					null, null, 1, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 3
	// Test objective: Test invalid input
	// Input(s): Journal id = 0
	// Expected Output(s): Boolean = false

	public void test1103() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 1, 1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 4
	// Test objective: Test invalid input
	// Input(s): Journal id = -1
	// Expected Output(s): Boolean = false

	public void test1104() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(-1, null, null, 1, 1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 5
	// Test objective: Test invalid input
	// Input(s): Journal id = MIN_INT
	// Expected Output(s): Boolean = false

	public void test1105() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(Integer.MIN_VALUE, null, null, 1, 1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 6
	// Test objective: Test Update valid input
	// Input(s): library number = MAX_INT
	// Expected Output(s): Boolean = true

	public void test1116() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true, testObject.updateJournal(1, null, null,
					Integer.MAX_VALUE, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 7
	// Test objective: Test Update valid input
	// Input(s): library number = 1
	// Expected Output(s): Boolean = true

	public void test1117() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true,
					testObject.updateJournal(1, null, null, 1, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 8
	// Test objective: Test Update invalid input
	// Input(s): library number = -1
	// Expected Output(s): Boolean = false

	public void test1118() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, -1, 0, 0, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 9
	// Test objective: Test Update invalid input
	// Input(s): library number = MIN_INT
	// Expected Output(s): Boolean = false

	public void test1119() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, Integer.MIN_VALUE, 1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 10
	// Test objective: Test Update invalid input
	// Input(s): library number = 0
	// Expected Output(s): Boolean = false

	public void test1120() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 0, 1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 11
	// Test objective: Test Update invalid input
	// Input(s): ISBN = -1
	// Expected Output(s): Boolean = false

	public void test1271() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 0, -1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 12
	// Test objective: Test Update invalid input
	// Input(s): ISBN = Integer.MAX_VALUE
	// Expected Output(s): Boolean = false

	public void test1272() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 1, Integer.MAX_VALUE, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 13
	// Test objective: Test Update valid input
	// Input(s): ISBN = 1
	// Expected Output(s): Boolean = true

	public void test1273() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true,
					testObject.updateJournal(1, null, null, 1, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 14
	// Test objective: Test Update invalid input
	// Input(s): ISBN = 0
	// Expected Output(s): Boolean = false

	public void test1274() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 1, 0, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 15
	// Test objective: Test Update invalid input
	// Input(s): ISBN = Integer.MIN_VALUE
	// Expected Output(s): Boolean = false

	public void test1275() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 1, Integer.MIN_VALUE, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 16
	// Test objective: Test Update invalid input
	// Input(s): year = -1
	// Expected Output(s): Boolean = false
	public void test1313() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 1, 1, 1, -1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 17
	// Test objective: Test Update invalid input
	// Input(s): year = 0
	// Expected Output(s): Boolean = false

	public void test1314() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 1, 1, 1, 0);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 18
	// Test objective: Test Update valid input
	// Input(s): year = 1
	// Expected Output(s): Boolean = true

	public void test1315() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true,
					testObject.updateJournal(1, null, null, 0, 0, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 19
	// Test objective: Test Update invalid input
	// Input(s): year = Integer.MIN_VALUE
	// Expected Output(s): Boolean = false

	public void test1316() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 1, 1, 1, Integer.MIN_VALUE);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 20
	// Test objective: Test Update invalid input
	// Input(s): year = Integer.MAX_VALUE
	// Expected Output(s): Boolean = false

	public void test1317() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 1, 1, 1, Integer.MAX_VALUE);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 21
	// Test objective: Test Update invalid input
	// Input(s): RefNo = -1
	// Expected Output(s): Boolean = false
	public void test1213() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 1, 1, -1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 22
	// Test objective: Test Update invalid input
	// Input(s): RefNo = 0
	// Expected Output(s): Boolean = false

	public void test01214() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 1, 1, 0, -1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 23
	// Test objective: Test Update valid input
	// Input(s): RefNo = 1
	// Expected Output(s): Boolean = true

	public void test01215() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 1, 1, 1, 0);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 24
	// Test objective: Test Update invalid input
	// Input(s): RefNo = Integer.MIN_VALUE
	// Expected Output(s): Boolean = false

	public void test00216() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 1, 1, Integer.MIN_VALUE, 0);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 25
	// Test objective: Test Update invalid input
	// Input(s): RefNo = Integer.MAX_VALUE
	// Expected Output(s): Boolean = false

	public void test1204() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 1, 1, Integer.MAX_VALUE, 0);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 26
	// Test objective: Test valid input
	// Input(s): Journal id = 1
	// Expected Output(s): Boolean = true

	public void test1714() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true, testObject.addJournal(1, null, null, 1, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 27
	// Test objective: Test valid input
	// Input(s): Journal id = MAX_INT
	// Expected Output(s): Boolean = true

	public void test1715() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true, testObject.addJournal(Integer.MAX_VALUE, null,
					null, 1, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 28
	// Test objective: Test invalid input
	// Input(s): Journal id = 0
	// Expected Output(s): Boolean = false
	public void test1716() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 1, 1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 29
	// Test objective: Test invalid input
	// Input(s): Journal id = -1
	// Expected Output(s): Boolean = false

	public void test1717() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(-1, null, null, 1, 1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 30
	// Test objective: Test invalid input
	// Input(s): Journal id = MIN_INT
	// Expected Output(s): Boolean = false

	public void test1718() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(Integer.MIN_VALUE, null, null, 1, 1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 31
	// Test objective: Test Update valid input
	// Input(s): library number = MAX_INT
	// Expected Output(s): Boolean = true

	public void test1756() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true, testObject.addJournal(1, null, null,
					Integer.MAX_VALUE, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 32
	// Test objective: Test Update valid input
	// Input(s): library number = 1
	// Expected Output(s): Boolean = true

	public void test1757() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true, testObject.addJournal(1, null, null, 1, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 33
	// Test objective: Test Update invalid input
	// Input(s): library number = -1
	// Expected Output(s): Boolean = false

	public void test1758() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, -1, 0, 0, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 34
	// Test objective: Test Update invalid input
	// Input(s): library number = MIN_INT
	// Expected Output(s): Boolean = false

	public void test1759() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, Integer.MIN_VALUE, 1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 35
	// Test objective: Test Update invalid input
	// Input(s): library number = 0
	// Expected Output(s): Boolean = false

	public void test1760() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 0, 1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 36
	// Test objective: Test Update invalid input
	// Input(s): ISBN = -1
	// Expected Output(s): Boolean = false

	public void test0221() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 0, -1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 37
	// Test objective: Test Update invalid input
	// Input(s): ISBN = Integer.MAX_VALUE
	// Expected Output(s): Boolean = false

	public void test0222() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 1, Integer.MAX_VALUE, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 38
	// Test objective: Test Update valid input
	// Input(s): ISBN = 1
	// Expected Output(s): Boolean = true

	public void test0223() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true, testObject.addJournal(1, null, null, 1, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 39
	// Test objective: Test Update invalid input
	// Input(s): ISBN = 0
	// Expected Output(s): Boolean = false

	public void test0224() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 1, 0, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 40
	// Test objective: Test Update invalid input
	// Input(s): ISBN = Integer.MIN_VALUE
	// Expected Output(s): Boolean = false

	public void test0225() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 1, Integer.MIN_VALUE, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 41
	// Test objective: Test Update invalid input
	// Input(s): year = -1
	// Expected Output(s): Boolean = false
	public void test0323() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 1, 1, 1, -1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 42
	// Test objective: Test Update invalid input
	// Input(s): year = 0
	// Expected Output(s): Boolean = false

	public void test00324() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 1, 1, 1, 0);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 43
	// Test objective: Test Update valid input
	// Input(s): year = 1
	// Expected Output(s): Boolean = true

	public void test00325() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true, testObject.addJournal(1, null, null, 0, 0, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 44
	// Test objective: Test Update invalid input
	// Input(s): year = Integer.MIN_VALUE
	// Expected Output(s): Boolean = false

	public void test0326() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 1, 1, 1, Integer.MIN_VALUE);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 45
	// Test objective: Test Update invalid input
	// Input(s): year = Integer.MAX_VALUE
	// Expected Output(s): Boolean = false

	public void test0327() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 1, 1, 1, Integer.MAX_VALUE);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 46
	// Test objective: Test Update invalid input
	// Input(s): RefNo = -1
	// Expected Output(s): Boolean = false
	public void test0233() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 1, 1, -1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 47
	// Test objective: Test Update invalid input
	// Input(s): RefNo = 0
	// Expected Output(s): Boolean = false

	public void test00234() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 1, 1, 0, -1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 48
	// Test objective: Test Update valid input
	// Input(s): RefNo = 1
	// Expected Output(s): Boolean = true

	public void test00235() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 1, 1, 1, 0);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 49
	// Test objective: Test Update invalid input
	// Input(s): RefNo = Integer.MIN_VALUE
	// Expected Output(s): Boolean = false

	public void test00236() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 1, 1, Integer.MIN_VALUE, 0);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}

	// Test number: 50
	// Test objective: Test Update invalid input
	// Input(s): RefNo = Integer.MAX_VALUE
	// Expected Output(s): Boolean = false

	public void test0237() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.addJournal(0, null, null, 1, 1, Integer.MAX_VALUE, 0);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid value entered", e.getMessage());
		}
	}
}
