package testing;

import dao.DaoJournal;
import junit.framework.TestCase;
import testing.ExceptionHandlerClass;

public class JournalTest extends TestCase {
	// Test number: 3
	// Test objective: Test valid input
	// Input(s): Book id = 1
	// Expected Output(s): Boolean = true
	public void test0001() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true,
					testObject.updateJournal(1, null, null, 0, 0, 0, 0));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 2
	// Test objective: Test valid input
	// Input(s): Book id = MAX_INT
	// Expected Output(s): Boolean = true
	// Added by: Shane
	public void test0002() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true, testObject.updateJournal(Integer.MAX_VALUE,
					null, null, 1, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 3
	// Test objective: Test invalid input
	// Input(s): Book id = 0
	// Expected Output(s): Boolean = false
	public void test0003() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 1, 1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals(
					"InvalidJournal ID entered, please enter a value greater than zero",
					e.getMessage());
		}
	}

	// Test number: 4
	// Test objective: Test invalid input
	// Input(s): Book id = -1
	// Expected Output(s): Boolean = false
	// Added by: Shane
	public void test0004() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(-1, null, null, 0, 0, 0, 0);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid Journal ID entered", e.getMessage());
		}
	}

	// Test number: 5
	// Test objective: Test invalid input
	// Input(s): Book id = MIN_INT
	// Expected Output(s): Boolean = false
	// Added by: Shane
	public void test0005() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(Integer.MIN_VALUE, null, null, 1, 1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid Journal ID entered", e.getMessage());
		}
	}

	// Test number: 6
	// Test objective: Test valid input
	// Input(s): Book id = 1
	// Expected Output(s): Boolean = true
	public void test0006() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true, testObject.addJournal(1, null, null, 0, 0, 0, 0));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 7
	// Test objective: Test valid input
	// Input(s): Book id = MAX_INT
	// Expected Output(s): Boolean = true
	// Added by: Shane
	public void test0007() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true, testObject.updateJournal(Integer.MAX_VALUE,
					null, null, 1, 1, 1, 1));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here ... no exception expected");
		}
	}

	// Test number: 8
	// Test objective: Test invalid input
	// Input(s): Book id = 0
	// Expected Output(s): Boolean = false
	public void test0008() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(0, null, null, 1, 1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals(
					"InvalidJournal ID entered, please enter a value greater than zero",
					e.getMessage());
		}
	}

	// Test number: 9
	// Test objective: Test invalid input
	// Input(s): Book id = -1
	// Expected Output(s): Boolean = false
	// Added by: Shane
	public void test0009() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(-1, null, null, 0, 0, 0, 0);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid Journal ID entered", e.getMessage());
		}
	}

	// Test number: 10
	// Test objective: Test invalid input
	// Input(s): Book id = MIN_INT
	// Expected Output(s): Boolean = false
	// Added by: Shane
	public void test00010() throws Exception {
		DaoJournal testObject = new DaoJournal();
		try {
			testObject.updateJournal(Integer.MIN_VALUE, null, null, 1, 1, 1, 1);
			fail("Should not get here .. Exception Expected");
		} catch (ExceptionHandlerClass e) {
			assertEquals("Invalid Journal ID entered", e.getMessage());
		}
	}
}
