package testing;

import ConnectDatabase.Conn_db;
import dao.DaoBook;
import dao.DaoJournal;
import junit.framework.TestCase;

public class PatrycjaTests extends TestCase {
	// Test Number : 1
	// Test Objective: Test Valid connection name
	// Input(s): dbName = "jdbc:mysql://127.0.0.1:3307/Agile"
	// Expected output(s): true

	public void test0001() {
		Conn_db testObject = new Conn_db();

		try {
			assertEquals(true,
					testObject.getConn("jdbc:mysql://127.0.0.1:3307/Agile"));
		} catch (ExceptionHandlerClass e) {
			fail("Should not reach here");
		}
	}

	// Test Number : 2
	// Test Objective: Test Invalid connection name
	// Input(s): dbName = "jdbc:mysql://127.0.0.1:3307/Database"
	// Expected output(s): Exception

	public void test0002() {
		Conn_db testObject = new Conn_db();

		try {
			testObject.getConn("jdbc:mysql://127.0.0.1:3307/failtest");
		} catch (ExceptionHandlerClass e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			assertEquals("Wrong Database", e.getMessage());
		}

	}

	// Test Number : 3
	// Test Objective: Test Valid Book name
	// Input(s): name = "Agile"
	// Expected output(s): true

	public void test0003() {
		DaoBook testObject = new DaoBook();
		try {
			assertEquals(true, testObject.searchBook("Agile"));
		} catch (ExceptionHandlerClass e) {
			e.getMessage();
		}

	}

	// Test Number : 4
	// Test Objective: Test inValid Book name
	// Input(s): name = "NoSQL"
	// Expected output(s): Exception

	public void test0004() {
		DaoBook testObject = new DaoBook();
		try {
			testObject.searchBook("NoSQL");
		} catch (ExceptionHandlerClass e) {
			// e.getMessage();
			assertEquals("Wrong Book name", e.getMessage());

		}
	}

	// Test Number : 5
	// Test Objective: Test Valid Journal name
	// Input(s): name = "JUnit"
	// Expected output(s): true

	public void test0005() {
		DaoJournal testObject = new DaoJournal();
		try {
			assertEquals(true, testObject.searchJournal("JUnit"));
		} catch (Exception e) {
			fail("this should not reach here");
		}

	}

	// Test Number : 6
	// Test Objective: Test inValid Journal name
	// Input(s): name = "Agile"
	// Expected output(s): Exception

	public void test0006() throws ExceptionHandlerClass {
		DaoJournal testObject = new DaoJournal();
		testObject.searchJournal("Agile");
	}

}
