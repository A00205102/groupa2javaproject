package classFolder;

public class Journal {
	public int Id;
	public String name;
	public String Author;
	public int LibraryNo;
	public int ISBN_No;
	public int Ref_No;
	public int release_Year;

	// constructor
	public Journal(int id, String name, String author, int Library_No,
			int ISBN_NO, int Ref_NO, int realease_Y) {
		this.Id = id;
		this.name = name;
		this.Author = author;
		this.LibraryNo = Library_No;
		this.ISBN_No = ISBN_NO;
		this.Ref_No = Ref_NO;
		this.release_Year = realease_Y;
	}

	public Journal() {

	}

	// getters & setters
	public int getID() {
		return Id;
	}

	public void setID(int id) {
		Id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor() {
		return Author;
	}

	public void setAuthor(String Author) {
		this.Author = Author;
	}

	public int getLibraryNo() {
		return LibraryNo;
	}

	public void setLibraryNo(int LibraryNo) {
		this.LibraryNo = LibraryNo;
	}

	public int getISBN_No() {
		return ISBN_No;
	}

	public void setISBN_No(int ISBN_No) {
		this.ISBN_No = ISBN_No;
	}

	public int getRef_No() {
		return Ref_No;
	}

	public void setRef_No(int Ref_No) {
		this.Ref_No = Ref_No;
	}

	public int getrelease_Year() {
		return release_Year;
	}

	public void setrelease_Year(int release_Year) {
		this.release_Year = release_Year;
	}

	// methods
}