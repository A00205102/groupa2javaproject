package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JOptionPane;

import classFolder.RequestFromDB;
import dao.DaoRequest;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.SQLException;

import javax.swing.JTextField;
import javax.swing.JLabel;

import testing.ExceptionHandlerClass;

public class AdminSendRequestNotification {

	private JFrame frame;
	private JTextField bookTextField;
	private JTextField journalTextField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdminSendRequestNotification window = new AdminSendRequestNotification();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AdminSendRequestNotification() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 376, 315);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JButton sendBook = new JButton("Send Notification");
		sendBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int id = Integer.parseInt(bookTextField.getText());
				RequestFromDB b;
				try {
					b = DaoRequest.bookRequestFromDb(id);
					DaoRequest.sendEmailNotificationForRequest(b);
				} catch (ClassNotFoundException | SQLException e) {
					e.printStackTrace();
				} catch (ExceptionHandlerClass e) {
					JOptionPane.showMessageDialog(sendBook, e);
				}
			}
		});
		sendBook.setBounds(101, 79, 163, 23);
		frame.getContentPane().add(sendBook);

		bookTextField = new JTextField();
		bookTextField.setBounds(79, 45, 200, 23);
		frame.getContentPane().add(bookTextField);
		bookTextField.setColumns(10);

		JLabel lblEnterIdOf = new JLabel(
				"Enter Id of book that is back in stock to alert students:");
		lblEnterIdOf.setBounds(10, 11, 359, 23);
		frame.getContentPane().add(lblEnterIdOf);

		JLabel lblEnterIdOf_1 = new JLabel(
				"Enter Id of journal that is back in stock to alert students:");
		lblEnterIdOf_1.setBounds(10, 130, 332, 23);
		frame.getContentPane().add(lblEnterIdOf_1);

		journalTextField = new JTextField();
		journalTextField.setBounds(79, 164, 200, 20);
		frame.getContentPane().add(journalTextField);
		journalTextField.setColumns(10);

		JButton sendJournal = new JButton("Send Notification");
		sendJournal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int id = Integer.parseInt(journalTextField.getText());
				RequestFromDB b;
				try {
					b = DaoRequest.journalRequestFromDb(id);
					DaoRequest.sendEmailNotificationForRequest(b);
				} catch (ClassNotFoundException | SQLException e) {
					e.printStackTrace();
				} catch (ExceptionHandlerClass e) {
					JOptionPane.showMessageDialog(sendJournal, e);
				}
			}
		});
		sendJournal.setBounds(101, 195, 163, 23);
		frame.getContentPane().add(sendJournal);

		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			@SuppressWarnings("static-access")
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
				AdminMenu menu = new AdminMenu();
				menu.main(null);
			}
		});
		btnBack.setBounds(142, 242, 89, 23);
		frame.getContentPane().add(btnBack);
	}
}
